FROM maven:3.6.3-jdk-11

ENV TZ="Asia/Ho_Chi_Minh"

WORKDIR /src/app/

COPY  target /src/app

RUN ls

CMD  ["java", "-Xms512m", "-Xmx512m", "-Dspring.profiles.active=dev", "-jar"  ,"/src/app/target/approval-limits-0.0.1-SNAPSHOT.jar"]
