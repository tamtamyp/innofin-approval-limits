package vn.asim.innofin.approval.repo;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.entity.RequestLimitAcceptance;

import java.util.List;

@Repository
public interface RequestLimitAcceptanceRepo extends PagingAndSortingRepository<RequestLimitAcceptance, Long> {
    List<RequestLimitAcceptance> findAllByRequestLimitIdIn(List<Long> ids, Sort sort);

    void deleteAllByRequestLimitIdIn(List<Long> requestIds);

    List<RequestLimitAcceptance> findAllByRequestLimitId(Long requestLimitId, Sort sort);

    void deleteByRequestLimitId(Long requestLimitId);
}
