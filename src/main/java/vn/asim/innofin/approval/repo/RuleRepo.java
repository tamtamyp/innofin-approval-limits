package vn.asim.innofin.approval.repo;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.entity.Rule;

import java.util.List;


@Repository
public interface RuleRepo extends PagingAndSortingRepository<Rule, Long>, JpaSpecificationExecutor<Rule> {

    public List<Rule> findAllByLimitType(LimitType limitType, Sort sort);

}