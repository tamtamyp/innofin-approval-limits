package vn.asim.innofin.approval.repo;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.entity.RuleLevel;

import java.util.List;

@Repository
public interface RuleLevelRepo extends PagingAndSortingRepository<RuleLevel, Long> {
    List<RuleLevel> findAllByRuleId(Long ruleId, Sort sort);

    List<RuleLevel> findAllByRuleIdIn(List<Long> ruleIds, Sort sort);
}
