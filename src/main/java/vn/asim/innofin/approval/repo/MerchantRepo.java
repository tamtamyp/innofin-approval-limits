package vn.asim.innofin.approval.repo;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.entity.Merchant;
import java.util.List;

@Repository
public interface MerchantRepo extends JpaRepository<Merchant, String> {
    List<Merchant> findAllByTenant(String tenant, Sort sort);
}
