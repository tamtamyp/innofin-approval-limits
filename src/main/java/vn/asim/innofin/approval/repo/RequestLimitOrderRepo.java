package vn.asim.innofin.approval.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.entity.RequestLimitForOrder;
import java.util.List;

@Repository
public interface RequestLimitOrderRepo extends JpaRepository<RequestLimitForOrder, Long> {
    void deleteByRequestLimitId(Long requestLimitId);

    List<RequestLimitForOrder> findByRequestLimitId(Long requestLimitId);

    List<RequestLimitForOrder> findAllByRequestLimitIdIn(List<Long> requestLimitIds);

    List<RequestLimitForOrder> findAllByCodeIn(List<String> orderCodes);
    List<RequestLimitForOrder> findAllByCode(String orderCodes);
    List<RequestLimitForOrder> findAllByCodeInAndRequestLimitIdIsNot(List<String> orderCodes, Long requestLimitId);
}
