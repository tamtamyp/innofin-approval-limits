package vn.asim.innofin.approval.repo;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.asim.innofin.approval.entity.FileData;

@Repository
public interface FileDataRepo extends JpaRepository<FileData, Long> {
    public FileData findFileDataByFileCode(String code);

    public List<FileData> findFileDataByRequestLimitId(Long requestLimitId);

    public void deleteFileDataByRequestLimitId(Long requestLimitId);

    public void deleteFileDataByRequestLimitIdAndFileCode(Long requestLimitId, String fileCode);
}
