package vn.asim.innofin.approval.repo;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.entity.RequestLimit;

import java.util.List;

@Repository
public interface ApprovalRequestRepo extends PagingAndSortingRepository<RequestLimit, Long>, JpaSpecificationExecutor<RequestLimit> {
    List<RequestLimit> findAllByIdIn(List<Long> ids);
    List<RequestLimit> findRequestLimitByCollectorId(String collectorId);

}
