package vn.asim.innofin.approval.repo;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.entity.ReferenceData;

@Repository
public interface ReferenceDataRepo extends PagingAndSortingRepository<ReferenceData, Long> {
}
