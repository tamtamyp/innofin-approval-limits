package vn.asim.innofin.approval.repo;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import vn.asim.innofin.approval.entity.TenantProfile;

import java.util.List;

public interface TenantRepo extends PagingAndSortingRepository<TenantProfile, Long> {

    List<TenantProfile> findAll(Sort sort);

    @CachePut(cacheNames = "tenants")
    TenantProfile findFirstByCode(String code);

    @Query(value = "SELECT * FROM APR_TENANT_SETTING t WHERE t.CODE = ?1", nativeQuery = true)
    @Cacheable(cacheNames = "tenants")
    TenantProfile findByCodeWithCache(String code);
}
