package vn.asim.innofin.approval.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.entity.User;

import java.util.Optional;
import java.util.List;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    List<User> findAllByRolesIn(List<String> roles);

    List<User> findAllByRoles(String roles);
}
