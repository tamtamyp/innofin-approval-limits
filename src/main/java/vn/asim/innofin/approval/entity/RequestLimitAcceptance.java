package vn.asim.innofin.approval.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Date;

@Table(name = "APR_REQUEST_LIMIT_ACCEPTANCE")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Data
public class RequestLimitAcceptance extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_request_limit_acceptance")
    @SequenceGenerator(name = "seq_request_limit_acceptance", sequenceName = "seq_request_limit_acceptance", allocationSize = 1)
    Long id;

    @Column(name = "level", nullable = false)
    long level;

    @Column(name = "isApproved", nullable = false)
    boolean isApproved;

    @Column(name = "approvedBy", nullable = false)
    String approvedBy;

    @Column(name = "approvedTime", nullable = true)
    Date approvedTime;

    @Column(name = "approvedByRole", nullable = true)
    String approvedByRole;

    @Column(name = "requestLimitId", nullable = false)
    long requestLimitId;

    @Column(name = "note", nullable = true)
    String note;
}

