package vn.asim.innofin.approval.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigInteger;

@Table(name = "APR_RULE_LEVEL")
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RuleLevel extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_rule_level")
    @SequenceGenerator(name = "seq_rule_level", sequenceName = "seq_rule_level", allocationSize = 1)
    Long id;
    @Column(name = "ruleId", nullable = false)
    Long ruleId;
    @Column(name = "level", nullable = false)
    long level;
    @Column(name = "limitValue", nullable = false)
    BigInteger limitValue;
    @Column(name = "approvedByRole", nullable = true)
    String approvedByRole;

}
