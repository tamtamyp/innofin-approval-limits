package vn.asim.innofin.approval.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.constants.enums.RequestLimitStatus;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Table(name = "APR_REQUEST_LIMIT")
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class RequestLimit extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_request_limit")
    @SequenceGenerator(name = "seq_request_limit", sequenceName = "seq_request_limit", allocationSize = 1)
    Long id;
    @Column(name = "requestSystem", nullable = false)
    String requestSystem;
    @Column(name = "limitType", nullable = false)
    @Enumerated(EnumType.STRING)
    LimitType limitType;

    @Column(name = "collectorId", nullable = false)
    String collectorId;
    @Column(name = "collectorName", nullable = true)
    String collectorName;

    @Column(name = "startWorkingDate", nullable = false)
    Date startWorkingDate;
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    RequestLimitStatus status;
    @Column(name = "requestValue", nullable = false)
    Long requestValue;
    @Column(name = "requestTotalValue", nullable = false)
    Long requestTotalValue;
    @Column(name = "matchingRuleId", nullable = false)
    Long matchingRuleId;
    @Column(name = "createdBy", nullable = false)
    String createdBy;
    @Column(name = "updatedBy", nullable = true)
    String updatedBy;
    @Column(name = "note", nullable = true)
    String note;
    @Column(name = "originCurrentLimit", nullable = true)
    Double originCurrentLimit;
    @Column(name = "originRemainLimit", nullable = true)
    Double originRemainLimit;

}

