package vn.asim.innofin.approval.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "APR_MERCHANTS")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Merchant {
    @Id
    String id;

    String code;

    String name;

    String logo;

    @Column(nullable = false)
    String tenant;
}
