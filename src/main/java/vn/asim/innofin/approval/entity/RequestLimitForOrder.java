package vn.asim.innofin.approval.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import vn.asim.innofin.approval.constants.enums.CashCollectionProcessStatus;
import vn.asim.innofin.approval.constants.enums.LimitType;

import javax.persistence.*;
import java.util.Date;

@Table(name = "APR_REQUEST_LIMIT_FOR_ORDER")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Data
public class RequestLimitForOrder {
    @Id
    String id;
    @Column( nullable = false)
    Long requestLimitId;

    @Column(unique = true)
    String code;
    String merchantName;
    String storeName;
    String bankName;
    Long amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date requestTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date requestEndTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date paymentTime;
    Long amountRecover; // số tiền thu hồi
    Long amountAllocate; // số tiền cấp phát
    boolean regainStatus; //Trạng thái thu hồi
    String historyId;
}
