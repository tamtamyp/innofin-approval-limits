package vn.asim.innofin.approval.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import vn.asim.innofin.approval.config.annotation.Exclude;

import javax.persistence.*;

@Table(name = "APR_TENANT_SETTING")
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class TenantProfile extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_system_setting")
    @SequenceGenerator(name = "seq_system_setting", sequenceName = "seq_system_setting", allocationSize = 1)
    @Exclude
    Long id;

    String code;
    String name;

    @Column(length = 256)
    String baseUrl;

    @Exclude
    @Column(length = 256)
    String description;

    @Column(length = 256)
    String clientId;

    @Column(length = 256)
    String secretKey;
}
