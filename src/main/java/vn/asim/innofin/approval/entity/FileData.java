package vn.asim.innofin.approval.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Table(name = "APR_FILE_DATA")
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class FileData {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_file_management")
    @SequenceGenerator(name = "seq_file_management", sequenceName = "seq_file_management", allocationSize = 1)
    Long id;

    private Long requestLimitId;
    private String fileName;

    private String fileCode;

}
