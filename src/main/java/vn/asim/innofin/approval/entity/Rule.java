package vn.asim.innofin.approval.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.utils.VNCharacterUtils;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "APR_RULE")
@Data
@EntityListeners(AuditingEntityListener.class)
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class Rule {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_rule")
    @SequenceGenerator(name = "seq_rule", sequenceName = "seq_rule", allocationSize = 1)
    Long id;
    @Column(name = "name", nullable = false)
    String name;
    @Column(name = "limitType", nullable = false)
    LimitType limitType;
    @Column(name = "effectiveDate", nullable = false)
    Date effectiveDate;
    @Column(name = "expiredDate", nullable = false)
    Date expiredDate;

    @Column(name = "createdBy", nullable = false)
    String createdBy;
    @Column(name = "nameNoAccent", nullable = false)
    private String nameNoAccent;

    public void setName(String name) {
        this.name = name;
        if (name != null) {
            this.nameNoAccent = VNCharacterUtils.removeAccent(name).toLowerCase();
        } else {
            this.nameNoAccent = null;
        }
    }

}
