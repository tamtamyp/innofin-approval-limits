package vn.asim.innofin.approval.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@NoArgsConstructor
@MappedSuperclass
@SuperBuilder(toBuilder = true)
public class BaseEntity {

    @Getter
    @CreationTimestamp
    @Column(name = "createdDate", updatable = false)
    private Date createdDate;

    @Getter
    @Setter
    @UpdateTimestamp
    @Column(name = "updatedDate")
    private Date updatedDate;

}