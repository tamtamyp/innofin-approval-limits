package vn.asim.innofin.approval.controller.back.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.AppConstants;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.RuleDto;
import vn.asim.innofin.approval.dto.response.MatchingRuleRes;
import vn.asim.innofin.approval.dto.response.SuccessResponse;
import vn.asim.innofin.approval.exception.model.ServiceException;
import vn.asim.innofin.approval.service.RuleService;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.InputStream;
import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.List;

@Tag(name = "rule-backend")
@RestController("RuleTemplateBackend")
@RequestMapping(path = "/b/v1/approval-rules")
@Slf4j
@Validated
@RequiredArgsConstructor
public class RuleController extends BaseController {

    private final RuleService ruleService;

    @Operation(summary = "Find the matching rule")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = MatchingRuleRes.class)))})
    @GetMapping("match")
    public ResponseEntity<MatchingRuleRes> matchingRule(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "Type of limit"
                    , example = "INDIVIDUAL")
            @RequestParam(value = "limitType", required = true) LimitType limitType,

            @Parameter(description = "Request value"
                    , example = "30000000")
            @RequestParam(value = "requestValue", required = true) Long requestTotalValue
    ) {
        MatchingRuleRes res = ruleService.findMatchingRule(limitType, requestTotalValue);
        if (res == null) {
            throw new ServiceException("No Rule was matched");
        }
        return ResponseEntity.ok(res);
    }

    @Operation(summary = "Search rules")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = PageSearchRuleRes.class)))})
    @GetMapping
    public ResponseEntity<BaseListResponse<RuleDto>> searchRules(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "search like by name")
            @RequestParam(value = "name", required = false) String name,

            @Parameter(description = "effective from. Format yyyy-MM-dd'T'HH:mm:ss.SSSXX")
            @RequestParam(value = "effectiveDateFrom", required = false) OffsetDateTime effectiveDateFrom,

            @Parameter(description = "effective to. Format yyyy-MM-dd'T'HH:mm:ss.SSSXX")
            @RequestParam(value = "effectiveDateTo", required = false) OffsetDateTime effectiveDateTo,
            @Min(0) @Parameter(description = "The number of pages to skip before starting to collect the result set")
            @Valid @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_STARTED_PAGE) Integer page,

            @Min(1) @Max(100) @Parameter(description = "The numbers of items to return", required = false)
            @Valid @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MAX_ELEMENT_PER_PAGE) Integer size) {
        return ResponseEntity.ok(
                BaseListResponse.ofOriginalPage(ruleService.search(name, effectiveDateFrom, effectiveDateTo, page, size))
        );
    }

    @Operation(summary = "Export Rules to excel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = PageSearchRuleRes.class)))})
    @GetMapping("export")
    @ResponseBody
    public ResponseEntity<InputStreamResource> exportRulesToExcel(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "search like by name")
            @RequestParam(value = "name", required = false) String name,

            @Parameter(description = "effective from. Format yyyy-MM-dd'T'HH:mm:ss.SSSXX")
            @RequestParam(value = "effectiveDateFrom", required = false) OffsetDateTime effectiveDateFrom,

            @Parameter(description = "effective to. Format yyyy-MM-dd'T'HH:mm:ss.SSSXX")
            @RequestParam(value = "effectiveDateTo", required = false) OffsetDateTime effectiveDateTo) {
        MediaType contentType = MediaType.TEXT_XML;
        InputStream streamResult = ruleService.exportToExcel(name, effectiveDateFrom, effectiveDateTo);
        return ResponseEntity.ok()
                .contentType(contentType)
                .body(new InputStreamResource(streamResult));
    }

    @Operation(summary = "Delete Rules")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = SuccessResponse.class)))})
    @DeleteMapping
    public ResponseEntity<SuccessResponse> deleteRules(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "List id to delete")
            @RequestParam(value = "ids", required = true) @NotEmpty List<@NotBlank String> ids) {
        ruleService.deleteBatch(ids);
        return ResponseEntity.ok(SuccessResponse.builder()
                .code("200")
                .message("Delete Sucessfully")
                .build()
        );
    }

    //TODO API create rule

    //TODO API update rule

    class PageSearchRuleRes extends BaseListResponse<RuleDto> {
    }
}
