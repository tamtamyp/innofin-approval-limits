package vn.asim.innofin.approval.controller.pub.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vn.asim.innofin.approval.config.annotation.NoLogging;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.request.LoginReqDto;
import vn.asim.innofin.approval.dto.response.APIExceptionDto;
import vn.asim.innofin.approval.service.UserService;


@Tag(name = "account-public")
@RestController("AccountPublicController")
@RequestMapping(path = "/p/v1/account")
@Slf4j
@Validated
@RequiredArgsConstructor
public class UserController extends BaseController {

    private final UserService userService;

    @Operation(summary = "Login")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Response is JWT", content = @Content(schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = APIExceptionDto.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = APIExceptionDto.class))),
            @ApiResponse(responseCode = "500", description = "Unhandled server error", content = @Content(schema = @Schema(implementation = APIExceptionDto.class)))})
    @PostMapping(value = "login",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public ResponseEntity<String> login(LoginReqDto loginReqDto) {
        return ResponseEntity.ok(userService.login(loginReqDto.getUsername(), loginReqDto.getPassword()));
    }


    @Operation(summary = "Logout")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Logout", content = @Content(schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = APIExceptionDto.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = APIExceptionDto.class))),
            @ApiResponse(responseCode = "500", description = "Unhandled server error", content = @Content(schema = @Schema(implementation = APIExceptionDto.class)))})
    @PostMapping(value = "logout",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    )
    public ResponseEntity<String> logout(JwtInfo jwt) {
        userService.logout(jwt);
        return ResponseEntity.ok("Logout successfully");
    }


}
