package vn.asim.innofin.approval.controller.back.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.AppConstants;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.response.GroupCollectorResDto;
import vn.asim.innofin.approval.dto.response.MerchantDto;
import vn.asim.innofin.approval.service.OrderService;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Tag(name = "merchants-backend")
@RestController("Merchants Backend")
@RequestMapping(path = "/b/v1/merchants")
@Slf4j
@Validated
@RequiredArgsConstructor
public class MerchantController extends BaseController {
    private final OrderService orderService;

    @Operation(summary = "Get all merchants")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(array = @ArraySchema(schema = @Schema(implementation = MerchantDto.class))))})
    @GetMapping("")
    public ResponseEntity<List<MerchantDto>> getGroup(
            @RequestParam(value = "requestSystem", required = true)
            @NotBlank String requestSystem) {

        return ResponseEntity.ok(orderService.getAllMerchants(requestSystem));
    }




}
