package vn.asim.innofin.approval.controller.back.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.response.TenantResDto;
import vn.asim.innofin.approval.service.TenantService;

import javax.validation.constraints.NotNull;
import java.util.List;

@Tag(name = "tenant-backend")
@RestController("TenantBackend")
@RequestMapping(path = "/b/v1/tenants")
@Slf4j
@Validated
@RequiredArgsConstructor
public class TenantController extends BaseController {

    private final TenantService tenantService;

    @Operation(summary = "Get all system")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = TenantResDto.class)))})
    @GetMapping
    public ResponseEntity<List<TenantResDto>> getAll(
            @Parameter(hidden = true) @NotNull JwtInfo jwt
    ) {
        return ResponseEntity.ok(tenantService.getAllTenants());
    }
}
