package vn.asim.innofin.approval.controller.back.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.AppConstants;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.response.CollectorResDto;
import vn.asim.innofin.approval.dto.response.GroupCollectorResDto;
import vn.asim.innofin.approval.service.CollectorService;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Tag(name = "collector-backend")
@RestController("Collector Backend")
@RequestMapping(path = "/b/v1/collectors")
@Slf4j
@Validated
@RequiredArgsConstructor
public class CollectorController extends BaseController {

    private final CollectorService collectorService;

    @Operation(summary = "Get Collectors")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = ResponseCollectorSearch.class)))})
    @GetMapping
    public ResponseEntity<BaseListResponse<CollectorResDto>> advancedSearchCollector(

            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @RequestParam(value = "requestSystem", required = true)
            @NotBlank String requestSystem,

            @Parameter(description = "collector name")
            @RequestParam(value = "name", required = false) String name,

            @Min(0) @Parameter(description = "The number of pages to skip before starting to collect the result set")
            @Valid @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_STARTED_PAGE) Integer page,

            @Min(1) @Max(100) @Parameter(description = "The numbers of items to return", required = false)
            @Valid @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MAX_ELEMENT_PER_PAGE) Integer size) {

        return ResponseEntity.ok(collectorService.searchCollector(requestSystem, name, page, size));
    }

    @Operation(summary = "Get Collector group")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = ResponseCollectorGroup.class)))})
    @GetMapping("groups")
    public ResponseEntity<BaseListResponse<GroupCollectorResDto>> getGroup(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @RequestParam(value = "requestSystem", required = true)
            @NotBlank String requestSystem,

            @Parameter(description = "collector group name")
            @RequestParam(value = "name", required = false) String name,

            @Min(0) @Parameter(description = "The number of pages to skip before starting to collect the result set")
            @Valid @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_STARTED_PAGE) Integer page,

            @Min(1) @Max(100) @Parameter(description = "The numbers of items to return", required = false)
            @Valid @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MAX_ELEMENT_PER_PAGE) Integer size) {

        return ResponseEntity.ok(collectorService.searchCollectorGroup(requestSystem, name, page, size));
    }

    private class ResponseCollectorSearch extends BaseListResponse<CollectorResDto> {
    }

    private class ResponseCollectorGroup extends BaseListResponse<GroupCollectorResDto> {
    }
}
