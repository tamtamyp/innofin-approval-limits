package vn.asim.innofin.approval.controller.back.v1;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.AppConstants;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.constants.enums.RequestLimitStatus;
import vn.asim.innofin.approval.constants.enums.Roles;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.request.CreateRqApprovalLimitDto;
import vn.asim.innofin.approval.dto.request.RqApproveDto;
import vn.asim.innofin.approval.dto.request.UpdateRqApprovalLimitDto;
import vn.asim.innofin.approval.dto.response.*;
import vn.asim.innofin.approval.exception.model.BadParameterException;
import vn.asim.innofin.approval.exception.model.NotExistException;
import vn.asim.innofin.approval.service.RequestLimitService;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Tag(name = "request-limit-backend")
@RestController("Request Approval Limits")
@RequestMapping(path = "/b/v1/request-limit")
@Slf4j
@Validated
@RequiredArgsConstructor
public class RequestLimitsController extends BaseController {


    private final RequestLimitService requestLimitService;


    @Operation(summary = "Advanced Search, Search the approval request limits")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = ResponseSearch.class)))})
    @GetMapping
    public ResponseEntity<BaseListResponse<RequestLimitRes>> advancedSearch(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "List Systems")
            @RequestParam(value = "requestSystem", required = false) List<@NotBlank String> requestSystem,

            @Parameter(description = "Type of limit"
                    , example = "INDIVIDUAL")
            @RequestParam(value = "limitType", required = false) LimitType limitType,

            @Parameter(description = "List CollectorId / GroupId")
            @RequestParam(value = "collectorIds", required = false) List<@NotBlank String> collectorIds,

            @Parameter(description = "List status to filter", array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = RequestLimitStatus.class)))
            @RequestParam(value = "status", required = false) @Valid List<RequestLimitStatus> status,

            @Parameter(description = "Created date from", example = "2023-01-26T05:48:36.411Z")
            @RequestParam(value = "createdDateFrom", required = false) @DateTimeFormat(pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
            Date createdDateFrom,

            @Parameter(description = "Created date to", example = "2023-04-26T05:48:36.411Z")
            @RequestParam(value = "createdDateTo", required = false) @DateTimeFormat(pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
            Date createdDateTo,

            @Parameter(description = "Limit From Value", example = "1000000")
            @RequestParam(value = "fromValue", required = false) BigInteger limitFrom,

            @Parameter(description = "Limit To Value", example = "9000000")
            @RequestParam(value = "toValue", required = false) BigInteger limitTo,

            @Min(0) @Parameter(description = "The number of pages to skip before starting to collect the result set")
            @Valid @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_STARTED_PAGE) Integer page,

            @Min(1) @Max(100) @Parameter(description = "The numbers of items to return", required = false)
            @Valid @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MAX_ELEMENT_PER_PAGE) Integer size) {
        Page<RequestLimitRes> pageResult = requestLimitService.advancedSearch(
                requestSystem,
                limitType,
                collectorIds,
                status,
                createdDateFrom,
                createdDateTo,
                limitFrom,
                limitTo,
                page,
                size
        );
        return ResponseEntity.ok(BaseListResponse.ofOriginalPage(pageResult));
    }

    @Operation(summary = "Get details of the approval request limits")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = RequestLimitRes.class)))})
    @GetMapping("{requestLimitId}")
    public ResponseEntity<RequestLimitRes> getDetails(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @PathVariable(value = "requestLimitId", required = true) Long requestLimitId) {
        RequestLimitRes res = requestLimitService.getDetails(requestLimitId);
        if (res == null) {
            log.warn("Request Limit id {} is not exists", requestLimitId);
            throw new NotExistException("Request limit is not exist");
        }
        return ResponseEntity.ok(res);
    }

    @Operation(summary = "Create a request to increase the limit for a collector/ group collector")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = RequestLimitRes.class)))})
    @PostMapping
    public ResponseEntity<RequestLimitRes> createRequest(
        @Parameter(hidden = true) @NotNull JwtInfo jwt,
            @RequestBody @Valid CreateRqApprovalLimitDto requestApprovalLimits) {
        if(!jwt.getRoles().contains(Roles.OPS.getRoleName())){
            throw new BadParameterException("Permission is not allow");
        }
        return ResponseEntity.ok(requestLimitService.create(jwt, requestApprovalLimits));
    }

    @Operation(summary = "Update a request to increase the limit for a collector/ group collector")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = RequestLimitRes.class)))})
    @PutMapping("{requestLimitId}")
    public ResponseEntity<RequestLimitRes> updateRequest(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,
            @PathVariable(value = "requestLimitId", required = true) Long requestLimitId,
            @RequestBody @Valid UpdateRqApprovalLimitDto requestApprovalLimits) {
        if(!jwt.getRoles().contains(Roles.OPS.getRoleName())){
            throw new BadParameterException("Permission is not allow");
        }
        return ResponseEntity.ok(requestLimitService.update(jwt, requestLimitId, requestApprovalLimits));
    }

    @Operation(summary = "Delete Request")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = SuccessResponse.class)))})
    @DeleteMapping("{requestIds}")
    public ResponseEntity<SuccessResponse> deleteRequestLimit(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,
            @PathVariable(value = "requestIds", required = true) @NotEmpty List<@NotNull Long> requestIds) {
        if(!jwt.getRoles().contains(Roles.OPS.getRoleName())){
            throw new BadParameterException("Permission is not allow");
        }
        requestLimitService.deleteBatch(requestIds);
        return ResponseEntity.ok(SuccessResponse.builder()
                .code("200")
                .message("Delete successfully")
                .build()
        );
    }



    @Operation(summary = "Export Request Limits to excel")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = RuleController.PageSearchRuleRes.class)))})
    @GetMapping("export")
    @ResponseBody
    public ResponseEntity<Resource> exportRequestLimitToExcel(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "List Systems")
            @RequestParam(value = "requestSystem", required = false) List<@NotBlank String> requestSystem,

            @Parameter(description = "Type of limit"
                    , example = "INDIVIDUAL")
            @RequestParam(value = "limitType", required = false) LimitType limitType,

            @Parameter(description = "List CollectorId / GroupId")
            @RequestParam(value = "collectorIds", required = false) List<@NotBlank String> collectorIds,

            @Parameter(description = "List status to filter", array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = RequestLimitStatus.class)))
            @RequestParam(value = "status", required = false) @Valid List<RequestLimitStatus> status,

            @Parameter(description = "Created date from", example = "2023-01-26T05:48:36.411Z")
            @RequestParam(value = "createdDateFrom", required = false) @DateTimeFormat(pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
            Date createdDateFrom,

            @Parameter(description = "Created date to", example = "2023-04-26T05:48:36.411Z")
            @RequestParam(value = "createdDateTo", required = false) @DateTimeFormat(pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
            Date createdDateTo,

            @Parameter(description = "Limit From Value", example = "1000000")
            @RequestParam(value = "fromValue", required = false) BigInteger limitFrom,

            @Parameter(description = "Limit To Value", example = "9000000")
            @RequestParam(value = "toValue", required = false) BigInteger limitTo) {
        MediaType contentType = MediaType.TEXT_XML;
        InputStream streamResult = requestLimitService.exportToExcel(
                requestSystem,
                limitType,
                collectorIds,
                status,
                createdDateFrom,
                createdDateTo,
                limitFrom,
                limitTo);
        InputStreamResource file = new InputStreamResource(streamResult);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(AppConstants.ONE_WORD_DATE_FORMAT);
        String fileName="export_"+ simpleDateFormat.format(new Date())+".xlsx";
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName)
                .contentType(contentType)
                .body(file);
    }

    @Operation(summary = "Update request approval limits")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = SuccessResponse.class)))})
    @PostMapping("approve")
    public ResponseEntity<SuccessResponse> approveRequest(
        @Parameter(hidden = true) @NotNull JwtInfo jwt,
            @RequestBody @Valid RqApproveDto requestApprove) {
        if(!(jwt.getRoles().contains(Roles.COO.getRoleName()) ||
                jwt.getRoles().contains(Roles.CEO.getRoleName()) ||
                jwt.getRoles().contains(Roles.MD.getRoleName()))){
            throw new BadParameterException("Permission is not allow");
        }
        requestLimitService.approve(jwt, requestApprove);
        return ResponseEntity.ok(SuccessResponse.builder()
                .message((requestApprove.getIsApprove() ? "Approve" : "Decline") + " is successfully")
                .build());
    }


    @Operation(summary = "Search history of approvals")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ResponseSearchHis.class))))})
    @GetMapping("histories")
    public ResponseEntity<BaseListResponse<ApprovalHistoryResDto>> approvalHistories(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "Type of limit"
                    , example = "INDIVIDUAL", required = true)
            @RequestParam(value = "limitType", required = true) @NotNull LimitType limitType,

            @Parameter(description = "Collector Id", required = true)
            @RequestParam(value = "collectorId", required = true) @NotBlank String collectorId,

            @Min(0) @Parameter(description = "The number of pages to skip before starting to collect the result set")
            @Valid @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_STARTED_PAGE) Integer page,

            @Min(1) @Max(100) @Parameter(description = "The numbers of items to return")
            @Valid @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MAX_ELEMENT_PER_PAGE) Integer size) {
        Page<ApprovalHistoryResDto> pageResult = requestLimitService.getApproveHistory(limitType, collectorId, page, size);
        return ResponseEntity.ok(BaseListResponse.ofOriginalPage(pageResult));
    }

    @Operation(summary = "Search history of payment")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(array = @ArraySchema(schema = @Schema(implementation = ResponseSearchPaymentHistory.class))))})
    @GetMapping("payment-histories")
    public ResponseEntity<BaseListResponse<DepositHistoryDto>> paymentHistories(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "request system"
                    , example = "requestSystem")
            @RequestParam(value = "requestSystem", required = true) String requestSystem,

            @Parameter(description = "Collector Id", required = true)
            @RequestParam(value = "collectorId", required = true) @NotBlank String collectorId,

            @Parameter(description = "Type of limit"
                    , example = "INDIVIDUAL", required = true)
            @RequestParam(value = "limitType", required = true) @NotNull LimitType limitType,

            @Min(0) @Parameter(description = "The number of pages to skip before starting to collect the result set")
            @Valid @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_STARTED_PAGE) Integer page,

            @Min(1) @Max(100) @Parameter(description = "The numbers of items to return")
            @Valid @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MAX_ELEMENT_PER_PAGE) Integer size) {
        return ResponseEntity.ok(requestLimitService.getPaymentHistory(requestSystem, collectorId, limitType, page, size));
    }

    @GetMapping("get-allocate_amount")
    public ResponseEntity<List<Long>> getAllocateAmount(
            @Parameter(description = "List Systems")
            @RequestParam(value = "requestSystem", required = false) String requestSystem,

            @Parameter(description = "Type of limit"
                    , example = "INDIVIDUAL")
            @RequestParam(value = "limitType", required = false) LimitType limitType,

            @Parameter(description = "CollectorId / GroupId")
            @RequestParam(value = "collectorId", required = false) String collectorId,

            @Parameter(description = "orders")
            @RequestParam(value = "orders", required = false) List<Double> orders
    ) {

        return ResponseEntity.ok(requestLimitService.getAllocateAmount(requestSystem, limitType, collectorId, orders));
    }

    class ResponseSearch extends BaseListResponse<RequestLimitRes> {
    }

    class ResponseSearchHis extends BaseListResponse<ApprovalHistoryResDto> {
    }

    class ResponseSearchPayment extends BaseListResponse<CashCollectionRes> {
    }

    class ResponseSearchPaymentHistory extends BaseListResponse<DepositHistoryDto> {
    }
}
