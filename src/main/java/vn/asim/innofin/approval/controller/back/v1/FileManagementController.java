package vn.asim.innofin.approval.controller.back.v1;


import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.response.FileDto;
import vn.asim.innofin.approval.service.FileDataService;
import vn.asim.innofin.approval.utils.FileUtils;
import org.springframework.core.io.Resource;

import java.io.IOException;

@Tag(name = "file-backend")
@RestController("File management")
@RequestMapping(path = "/b/v1/file-management")
@Slf4j
@Validated
@RequiredArgsConstructor
public class FileManagementController extends BaseController {

    private final FileDataService fileDataService;
    @GetMapping("download/{fileId}")
    public ResponseEntity<Resource> download(@PathVariable(name = "fileId") String fileId) throws IOException {
        FileDto fileDto = fileDataService.getFile(fileId);
        InputStreamResource resource = fileDto.getFileData();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+fileDto.getFileName());
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .headers(headers)
                .body(resource);
    }
}
