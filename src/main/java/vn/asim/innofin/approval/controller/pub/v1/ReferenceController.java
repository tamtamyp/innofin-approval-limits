package vn.asim.innofin.approval.controller.pub.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.asim.innofin.approval.constants.enums.ReferenceDataType;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.ReferenceDataDto;
import vn.asim.innofin.approval.dto.response.APIExceptionDto;
import vn.asim.innofin.approval.service.CommonService;
import java.util.List;


@Tag(name = "data-public")
@RestController("ReferenceDataController")
@RequestMapping(path = "/p/v1/reference-data")
@Slf4j
@Validated
@RequiredArgsConstructor
public class ReferenceController extends BaseController {

    private final CommonService commonService;

    @Operation(summary = "Get Dropdown List data")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Response is JWT", content = @Content(schema = @Schema(implementation = String.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = APIExceptionDto.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(implementation = APIExceptionDto.class))),
            @ApiResponse(responseCode = "500", description = "Unhandled server error", content = @Content(schema = @Schema(implementation = APIExceptionDto.class)))})
    @GetMapping
    public ResponseEntity<List<ReferenceDataDto>> getDropList(
            @Parameter(description = "Reference Data type")
            @RequestParam(value = "type", required = true) ReferenceDataType type) {
        return ResponseEntity.ok(commonService.getRef(type));
    }
}
