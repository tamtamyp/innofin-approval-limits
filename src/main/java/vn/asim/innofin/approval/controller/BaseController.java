package vn.asim.innofin.approval.controller;

import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BaseController {
}
