package vn.asim.innofin.approval.controller.back.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.AppConstants;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.controller.BaseController;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.response.CashCollectionRes;
import vn.asim.innofin.approval.service.OrderService;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.awt.*;

@Tag(name = "orders-backend")
@RestController("Orders Backend")
@RequestMapping(path = "/b/v1/orders")
@Slf4j
@Validated
@RequiredArgsConstructor
public class OrdersController extends BaseController {

    private final OrderService orderService;

    @Operation(summary = "Get orders")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful", content = @Content(schema = @Schema(implementation = PageSearchOrderAssigned.class)))})
    @GetMapping
    public ResponseEntity<BaseListResponse<CashCollectionRes>> getOrdersAssignedForCollector(
            @Parameter(hidden = true) @NotNull JwtInfo jwt,

            @Parameter(description = "request system"
                    , example = "requestSystem")
            @RequestParam(value = "requestSystem", required = true) @NotBlank String requestSystem,

            @Parameter(description = "collectorId. If this param is not appear, result is all new orders")
            @RequestParam(value = "collectorId", required = false) String collectorId,

            @Parameter(description = "Type of limit")
            @RequestParam(value = "limitType", required = false) LimitType limitType,

            @Parameter(description = "Order code to search")
            @RequestParam(value = "orderCode", required = false) String orderCode,

            @Parameter(description = "MerchantId")
            @RequestParam(value = "merchantId", required = false) String merchantId,

            @Min(0) @Parameter(description = "The number of pages to skip before starting to collect the result set")
            @Valid @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_STARTED_PAGE) Integer page,

            @Min(1) @Max(100) @Parameter(description = "The numbers of items to return", required = false)
            @Valid @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MAX_ELEMENT_PER_PAGE) Integer size) {
        return ResponseEntity.ok(orderService.getOrders(requestSystem, collectorId, limitType, orderCode, merchantId, page, size));
    }

    class PageSearchOrderAssigned extends BaseListResponse<CashCollectionRes> {
    }
}
