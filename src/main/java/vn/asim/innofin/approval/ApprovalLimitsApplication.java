package vn.asim.innofin.approval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import vn.asim.innofin.approval.entity.User;
import vn.asim.innofin.approval.repo.UserRepo;
import vn.asim.innofin.approval.service.EmailService;
import vn.asim.innofin.approval.utils.Utils;

import java.util.Date;
import java.util.HashMap;

@EnableAspectJAutoProxy
@SpringBootApplication
@EnableAsync
@EnableScheduling
@EnableJpaAuditing
@EnableCaching
public class ApprovalLimitsApplication {

    public static void main(String[] args) {
        ApplicationContext ctx =
                SpringApplication.run(ApprovalLimitsApplication.class, args);
//        initialUser(ctx);
    }

    private static void sendEmailTest(ApplicationContext ctx){
        EmailService emailService = ctx.getBean(EmailService.class);
        HashMap<String, Object> variables = new HashMap<>();
        variables.put("collectorName","Nguyễn Văn A");
        variables.put("amount","1,000,000,000");
        variables.put("total","2,000,000,000");
        variables.put("approvalName","Nguyễn Minh T");
        emailService.sendRequestApprovalEmail(new String[]{"quanbk216@gmail.com"},
                null, null, variables );
    }

    private static void initialUser(ApplicationContext ctx) {
        UserRepo repoUser = ctx.getBean(UserRepo.class);

        //INF_CEO
        repoUser.save(
                User.builder().lastLoginAt(new Date())
                        .roles("INF_CEO")
                        .username("ceo")
                        .password(Utils.encryptPassword("ceo@123"))
                        .fullname("A A A")
                        .email("quanbk216@gmail.com")
                        .build()
        );
        //INF_COO
        repoUser.save(
                User.builder().lastLoginAt(new Date())
                        .roles("INF_COO")
                        .username("coo")
                        .password(Utils.encryptPassword("coo@123"))
                        .fullname("A A A")
                        .email("quanbk216@gmail.com")
                        .build()
        );
        //INF_MD
        repoUser.save(
                User.builder().lastLoginAt(new Date())
                        .roles("INF_MD")
                        .username("md")
                        .password(Utils.encryptPassword("md@123"))
                        .fullname("A A A")
                        .email("quanbk216@gmail.com")
                        .build()
        );
        //INF_VIEW 1
        repoUser.save(
                User.builder().lastLoginAt(new Date())
                        .roles("INF_OPS")
                        .username("ops1")
                        .password(Utils.encryptPassword("ops1@123"))
                        .fullname("A A A")
                        .email("quanbk216@gmail.com")
                        .build()
        );
        //INF_VIEW 2
        repoUser.save(
                User.builder().lastLoginAt(new Date())
                        .roles("INF_OPS")
                        .username("ops2")
                        .password(Utils.encryptPassword("ops2@123"))
                        .fullname("A A A")
                        .email("quanbk216@gmail.com")
                        .build()
        );
        //INF_VIEW 3
        repoUser.save(
                User.builder().lastLoginAt(new Date())
                        .roles("INF_OPS")
                        .username("ops3")
                        .password(Utils.encryptPassword("ops3@123"))
                        .fullname("A A A")
                        .email("quanbk216@gmail.com")
                        .build()
        );
    }
}
