package vn.asim.innofin.approval.constants.enums;

import lombok.Getter;

@Getter
public enum Roles {
    CEO("INF_CEO"),
    COO("INF_COO"),
    MD("INF_MD"),
    OPS("INF_OPS");

    private final String roleName;

    Roles(String roleName) {
        this.roleName = roleName;
    }
}
