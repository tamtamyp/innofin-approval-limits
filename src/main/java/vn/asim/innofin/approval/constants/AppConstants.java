package vn.asim.innofin.approval.constants;

public class AppConstants {

    public static final String DEFAULT_MAX_ELEMENT_PER_PAGE = "20";
    public static final String DEFAULT_STARTED_PAGE = "0";
    public static final String JWT_SECRET_KEY = "rzkLyBy1pbzwktPAT6SL";

    public static final String ONE_WORD_DATE_FORMAT = "yyyyMMddHHmm";
    public static final int MAX_UPLOAD_SIZE = 1000;


}
