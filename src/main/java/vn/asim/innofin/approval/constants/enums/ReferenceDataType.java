package vn.asim.innofin.approval.constants.enums;

import lombok.Getter;

@Getter
public enum ReferenceDataType {

    ROLES("INF_ROLES"),
    LIMIT_TYPE("INF_LIMIT_TYPE"),
    REQUEST_STATUS("INF_REQUEST_STATUS");
    private final String name;

    ReferenceDataType(String name) {
        this.name = name;
    }
}
