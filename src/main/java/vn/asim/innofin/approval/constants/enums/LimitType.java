package vn.asim.innofin.approval.constants.enums;

import lombok.Getter;

@Getter
public enum LimitType {
    INDIVIDUAL("INDIVIDUAL"),
    GROUP("GROUP");

    private final String code;

    LimitType(String code) {
        this.code = code;
    }
}
