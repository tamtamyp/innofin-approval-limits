package vn.asim.innofin.approval.constants.enums;

import lombok.Getter;

@Getter
public enum RequestLimitStatus {
    IN_WAITING("IN_WAITING"),
    REJECTED("REJECTED"),
    APPROVED_LV1("APPROVED_LV1"),
    APPROVED_LV2("APPROVED_LV2"),
    APPROVED_LV3("APPROVED_LV3"),
    APPROVED_LV4("APPROVED_LV4"),
    APPROVED("APPROVED");

    private final String code;

    RequestLimitStatus(String code) {
        this.code = code;
    }
}
