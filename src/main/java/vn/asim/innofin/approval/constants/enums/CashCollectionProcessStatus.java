package vn.asim.innofin.approval.constants.enums;

import lombok.Getter;

@Getter
public enum CashCollectionProcessStatus {

    NEW(0),
    COLLECTOR_RECEIVED(1),
    START_COLLECT(2),
    REQUEST_MODIFY_STATEMENT(3),
    COMPLETED(4),
    CONFIRM_COLLECTION(5),
    CANCEL(-1),
    CANCEL_BY_DRIVER(-2),
    EXPIRED(-3),
    SCHEDULE(6);

    private final int code;

    CashCollectionProcessStatus(int code) {
        this.code = code;
    }
}

/**
 * New = 0,
 * CollectorReceived = 1,
 * StartCollect = 2,
 * RequestModifyStatement = 3,
 * Completed = 4,
 * ConfirmColection = 5,
 * Cancel = -1,
 * CancelByDriver = -2,
 * Expried = -3,
 * Schedule = 6
 */
