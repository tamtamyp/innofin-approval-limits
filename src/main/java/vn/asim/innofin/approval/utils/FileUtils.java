package vn.asim.innofin.approval.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.UrlResource;

import java.io.*;
import java.nio.file.Path;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import vn.asim.innofin.approval.exception.model.ServiceException;
import vn.asim.innofin.approval.exception.model.StorageException;
import vn.asim.innofin.approval.exception.model.StorageFileNotFoundException;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class FileUtils {

    @Value("${approval.file-store.path:/storage}")
    String fileStorePath;
    @PostConstruct
    public void setRootLocation(){
        rootLocation = Paths.get(fileStorePath);
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    private static Path rootLocation;

    public static Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file.toFile()));
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);
            }
        } catch (Exception e) {
            log.warn("Could not read file: + filename");
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    public static void save(String fileName, byte[] base64Encoded){
        try (OutputStream stream = new FileOutputStream(load(fileName).toFile())) {
            stream.write(base64Encoded);
        } catch (FileNotFoundException e) {
            log.warn("File {} not found",fileName);
            throw new StorageFileNotFoundException(
                    "Could not read file: " + fileName);
        } catch (IOException e) {
            throw new ServiceException("ERR when save file");
        }
    }
    private static Path load(String filename) {
        return rootLocation.resolve(filename);
    }
}
