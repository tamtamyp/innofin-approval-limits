package vn.asim.innofin.approval.utils;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import okhttp3.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
@Slf4j
public class RequestUtils {
    public static Gson gson = new Gson();
    private static OkHttpClient CLIENT;
    public static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");
    public static Headers EMPTY_HEADER = new Headers.Builder().build();
    static {
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };

        // Install the all-trusting trust manager
        final SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        try {
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            throw new RuntimeException(e);
        }
        // Create an ssl socket factory with our all-trusting manager
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
        builder.hostnameVerifier((hostname, session) -> true);
        ConnectionPool connectionPool = new ConnectionPool(10, 1000, TimeUnit.MILLISECONDS);
        CLIENT = builder
                .followRedirects(false)
                .retryOnConnectionFailure(true)
                // .addInterceptor(new RequestInterceptor())
                // .addInterceptor(new ResponseInterceptor())
                .connectionPool(connectionPool)
                .callTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build();
    }

    public static <T extends Object> T postJson(String tagMethod, String url, Headers headers, String bodyRequest, Class<T> clazz) throws Exception {
        RequestBody body = RequestBody.create(bodyRequest, MEDIA_TYPE);
        return post(tagMethod, url, headers, body, clazz);
    }

    public static <T extends Object> T postJson(String tagMethod, String url, String bodyRequest, Class<T> clazz) throws Exception {
//        System.out.println("= " + url);
//        System.out.println("= " + bodyRequest);
        RequestBody body = RequestBody.create(bodyRequest, MEDIA_TYPE);
        return post(tagMethod, url, EMPTY_HEADER, body, clazz);
    }

    public static <T extends Object> T postForm(String tagMethod, String url, HashMap<String, String> bodyRequest, Class<T> clazz) throws Exception {
        FormBody.Builder builder = new FormBody.Builder();
        bodyRequest.keySet().forEach(key -> {
            builder.add(key, bodyRequest.get(key));
        });
        RequestBody body = builder.build();
        return post(tagMethod, url, EMPTY_HEADER, body, clazz);
    }

    private static <T extends Object> T post(String tagMethod, String url, Headers headers, RequestBody body, Class<T> clazz) throws Exception {
      try {
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .headers(headers)
                    .tag(tagMethod)
                    .build();
            Response execute = CLIENT.newCall(request).execute();
            String response = execute.body().string();
            execute.close();
            return gson.fromJson(response, clazz);
        } catch (Exception e) {
            throw e;
        }  
    }

    public static <T extends Object> T putJson(String tagMethod, String url, String bodyRequest, Class<T> clazz) throws Exception {
        //        System.out.println("= " + url);
        //        System.out.println("= " + bodyRequest);
                RequestBody body = RequestBody.create(bodyRequest, MEDIA_TYPE);
                return put(tagMethod, url, EMPTY_HEADER, body, clazz);
            }

    public static <T extends Object> T putJson(String tagMethod, String url, Headers headers, String bodyRequest, Class<T> clazz) throws Exception {
                RequestBody body = RequestBody.create(bodyRequest, MEDIA_TYPE);
                return put(tagMethod, url, headers, body, clazz);
            }
    private static <T extends Object> T put(String tagMethod, String url, Headers headers, RequestBody body, Class<T> clazz) throws Exception {
        try {
              Request request = new Request.Builder()
                      .url(url)
                      .put(body)
                      .headers(headers)
                      .tag(tagMethod)
                      .build();
              Response execute = CLIENT.newCall(request).execute();
              String response = execute.body().string();
              execute.close();
              log.info(""+response);
              return gson.fromJson(response, clazz);
          } catch (Exception e) {
              throw e;
          }  
      }

    public static <T extends Object> T getJson(String tagMethod, String url, Class<T> clazz) throws Exception {
        return get(tagMethod, url, EMPTY_HEADER, clazz);
    }

    public static <T extends Object> T getJsonHeader(String tagMethod, String url, Headers headers, Class<T> clazz) throws Exception {
        return get(tagMethod, url, headers, clazz);
    }

    private static <T extends Object> T get(String tagMethod, String url, Headers headers, Class<T> clazz) throws Exception {
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .headers(headers)
                    .tag(tagMethod)
                    .build();
            Response execute = CLIENT.newCall(request).execute();
            String response = execute.body().string();
            execute.close();
            return gson.fromJson(response, clazz);
        } catch (Exception e) {
            throw e;
        }
    }

}
