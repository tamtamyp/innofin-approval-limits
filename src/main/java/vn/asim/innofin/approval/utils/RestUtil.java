package vn.asim.innofin.approval.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import okhttp3.Headers;
import vn.asim.innofin.approval.entity.TenantProfile;

public class RestUtil {

    private static final String HEADER_CLIENT_ID = "ClientId";
    private static final String HEADER_SECRET_KEY = "SecretKey";

    public static HttpHeaders createHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    public static HttpHeaders createHttpHeaderForTenant(TenantProfile tenantProfile){
        HttpHeaders httpHeaders = createHttpHeaders();
        httpHeaders.add(HEADER_CLIENT_ID, tenantProfile.getClientId());
        httpHeaders.add(HEADER_SECRET_KEY, tenantProfile.getSecretKey());
        return httpHeaders;
    }

    public static Headers createHeaderForTenant(TenantProfile tenantProfile){
        Headers header = new Headers.Builder()
                   .add(HEADER_CLIENT_ID, tenantProfile.getClientId())
                   .add(HEADER_SECRET_KEY, tenantProfile.getSecretKey())
                   .build();
        return header;
    }
}
