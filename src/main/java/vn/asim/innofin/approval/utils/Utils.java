package vn.asim.innofin.approval.utils;


import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import vn.asim.innofin.approval.config.annotation.NoLogging;
import vn.asim.innofin.approval.exception.model.ServiceException;

import javax.annotation.PostConstruct;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;

@Slf4j
@Component
public class Utils {
    private static final int PSW_STRENGTH = 10;

    private static final String PATTERN_CURENCY = "(\\d)(?=(\\d{3})+$)";
    public static String encryptPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] digest = md.digest();
            String myHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();
            return myHash;
        }catch (Exception e){
            log.error(e.getMessage());
            throw new ServiceException(e.getMessage());
        }
    }

    public static boolean checkPassword(String password, String encryptedPassword) {
        String hash = encryptPassword(password);
        return hash.equals(encryptedPassword);
    }

    private static Gson gson;

    public static Gson getGSonConverter() {
        if (gson == null) {
            ExclusionStrategy strategy = new ExclusionStrategy() {
                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }

                @Override
                public boolean shouldSkipField(FieldAttributes field) {
                    return field.getAnnotation(EqualsAndHashCode.Exclude.class) != null;
                }
            };

            gson = new GsonBuilder()
                    .addSerializationExclusionStrategy(strategy)
                    .create();
        }
        return gson;
    }

    public static String formatCurrency(String rawNumber){
        return rawNumber.replaceAll(PATTERN_CURENCY, "$1,");
    }

}
