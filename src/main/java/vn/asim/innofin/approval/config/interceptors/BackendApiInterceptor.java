package vn.asim.innofin.approval.config.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.exception.model.NotAuthorizedException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class BackendApiInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) {
        String authorizationHeader = request.getHeader("Authorization");

        if (authorizationHeader == null) {
            throw new NotAuthorizedException("Missing JWT");
        }

        try {
            JwtInfo jwtInfo = new JwtInfo(authorizationHeader);
        } catch (Exception e) {
            log.warn("BackendApiInterceptor - Invalid JWT {}", e.getMessage());
            throw new NotAuthorizedException("Invalid JWT");
        }
        return true;
    }
}
