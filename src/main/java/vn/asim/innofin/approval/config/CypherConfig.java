package vn.asim.innofin.approval.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Slf4j
public class CypherConfig {
    @Value("${approval.encrypted-password}")
    String encryptedPassword;


}
