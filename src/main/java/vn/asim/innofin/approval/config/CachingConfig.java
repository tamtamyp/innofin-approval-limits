package vn.asim.innofin.approval.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@Slf4j
public class CachingConfig {

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("tenants");
    }

    @CacheEvict(value = "tenants", allEntries = true)
    @Scheduled(fixedRateString = "${approval.caching.tenant}")
    @ConditionalOnProperty(value = "approval.caching.tenant", matchIfMissing = false, havingValue = "true")
    public void emptyTenantCache() {
        log.info("emptying Tenant cache");
    }
}
