package vn.asim.innofin.approval.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.utils.Utils;

import java.util.List;

@Aspect
@Component
@Slf4j
public class LoggingAspect {
    @Pointcut("execution(* vn.asim.innofin.approval.controller..*(..)) " +
            " || execution(* vn.asim.innofin.approval.client..*(..)) ")
    public void applicationPointcut() {
        //just point cut
    }

    @Pointcut("execution(* vn.asim.innofin.approval.service..*(..)) ")
    public void servicePointcut() {
        //just point cut
    }

    @Pointcut("!@annotation(vn.asim.innofin.approval.config.annotation.NoLogging)" +
            " && !@target(vn.asim.innofin.approval.config.annotation.NoLogging)")
    public void noLoggingPointcut() {
        //just point cut
    }

    @Around("applicationPointcut() && noLoggingPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        if (log.isInfoEnabled()) {
            log.info("=== Start: {}.{}() with argument[s] = {} ===============", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), getRequestJsonString(joinPoint.getArgs()));
        }
        Object result = joinPoint.proceed();
        if (log.isInfoEnabled()) {
            log.info("=== End: {}.{}() with result = {} ===============", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), getResponseJsonString(result));
        }
        return result;
    }

    private String getRequestJsonString(Object[] objs) {
        StringBuilder str = new StringBuilder();
        for (Object obj : objs) {
            String tmp = getObjectJsonString(obj);
            if (tmp != null) {
                str.append(tmp).append(", ");
            }
        }
        str.append("]");
        return str.toString();
    }

    private String getResponseJsonString(Object obj) {
        if (obj instanceof List) {
            return getListJsonString(obj);
        } else if (obj instanceof ResponseEntity) {
            ResponseEntity responseEntity = (ResponseEntity) obj;
            Object body = responseEntity.getBody();
            if (body instanceof List) {
                return getListJsonString(body);
            } else if (body instanceof BaseListResponse) {
                BaseListResponse baseListResponse = (BaseListResponse) body;
                return getObjectJsonString(baseListResponse.getPaginate());
            } else if (body instanceof Resource){
                Resource resourceBody = (Resource) body;
                return "{binary data ***}";
            }else {
                return getObjectJsonString(body);
            }
        } else {
            return getObjectJsonString(obj);
        }
    }

    private String getObjectJsonString(Object obj) {
        try {
            return Utils.getGSonConverter().toJson(obj);
        } catch (Throwable e) {
            return null;
        }
    }

    private String getListJsonString(Object obj) {
        try {
            List tmp = (List) obj;
            return "size: " + tmp.size();
        } catch (Throwable e) {
            return null;
        }
    }

    @Around("servicePointcut() && noLoggingPointcut()")
    public Object logAroundService(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info("===== Start: {}.{}()  =====", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName());
        }
        Object result = joinPoint.proceed();
        if (log.isInfoEnabled()) {
            log.info("===== End: {}.{}(), {} =====", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), "time execution is : " + (System.currentTimeMillis() - startTime) + "ms");
        }
        return result;
    }
}
