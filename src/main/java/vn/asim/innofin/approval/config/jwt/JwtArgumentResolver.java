package vn.asim.innofin.approval.config.jwt;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import vn.asim.innofin.approval.exception.model.NotAuthorizedException;

import javax.servlet.http.HttpServletRequest;

@NoArgsConstructor
@Slf4j
@Component
public class JwtArgumentResolver implements HandlerMethodArgumentResolver {

    private final String[] excludeUris = new String[]{"/p/v1/account/login", "/healthcheck", "healthCheck"};

    private boolean isUriExcluded(String uri) {
        for (String s : excludeUris) {
            if (uri.startsWith(s)) {
                return true;
            }
        }
        return false;
    }

    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(JwtInfo.class);
    }

    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        String authorizationHeader = webRequest.getHeader("authorization");

        if (authorizationHeader == null) {
            if (!isUriExcluded(request.getRequestURI())) {
                throw new NotAuthorizedException("Missing JWT");
            }
            return null;
        }

        try {
            return new JwtInfo(authorizationHeader);
        } catch (Exception e) {
            log.error("Invalid JWT: {}", authorizationHeader);
            if (!isUriExcluded(request.getRequestURI())) {
                throw new NotAuthorizedException("Invalid JWT");
            }
            return null;
        }
    }
}
