package vn.asim.innofin.approval.config.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import vn.asim.innofin.approval.constants.AppConstants;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Slf4j
public class JwtInfo {

    @Schema(hidden = true)
    public static final String USER_FIELD = "sub";
    @Schema(hidden = true)
    public static final String ROLES_FIELD = "roles";

    @Schema(hidden = true)
    public static final String EMAIL_FIELD = "email";

    @Schema(hidden = true)
    public static final String FULLNAME_FIELD = "fullName";
    @Schema(hidden = true)
    private String user;

    @Schema(hidden = true)
    private String email;

    @Schema(hidden = true)
    private String fullName;

    @Schema(hidden = true)
    private Set<String> roles = new HashSet<>();

    public JwtInfo(String authorizationHeader) {
        String[] splitString = authorizationHeader.split("Bearer ");
        String jwtBase64 = splitString.length == 2 ? splitString[1] : "";
        DecodedJWT jwt = JWT.decode(jwtBase64); // decode empty string will throw JWTDecodeException
        JWT.require(Algorithm.HMAC512(AppConstants.JWT_SECRET_KEY)).build().verify(jwt);
        this.user =
                jwt.getClaims().get(USER_FIELD) != null ? jwt.getClaims().get(USER_FIELD).asString() : null;

        this.email =
                jwt.getClaims().get(EMAIL_FIELD) != null ? jwt.getClaims().get(EMAIL_FIELD).asString() : null;

        this.fullName =
                jwt.getClaims().get(FULLNAME_FIELD) != null ? jwt.getClaims().get(FULLNAME_FIELD).asString() : null;

        if (StringUtils.isEmpty(user)) {
            throw new RuntimeException("Required field (" + USER_FIELD + ") is not exist in jwt token");
        }
        if (StringUtils.isEmpty(email)) {
            throw new RuntimeException("Required field (" + EMAIL_FIELD + ") is not exist in jwt token");
        }
        if (StringUtils.isEmpty(fullName)) {
            throw new RuntimeException("Required field (" + FULLNAME_FIELD + ") is not exist in jwt token");
        }

        Date expiredDate = jwt.getExpiresAt();
        if(expiredDate.before(new Date())){
            log.warn("Token is expired");
            throw new TokenExpiredException("Expired token", expiredDate.toInstant());
        }

        Claim groupData = jwt.getClaims().get(ROLES_FIELD);
        if (groupData != null) {
            this.roles.addAll(groupData.asList(String.class));
        }
    }
}
