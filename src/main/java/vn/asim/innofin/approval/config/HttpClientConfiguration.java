package vn.asim.innofin.approval.config;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HttpClientConfiguration {
    @Value("${spring.rest.client.read-timeout}")
    private int readTimeout;

    @Value("${spring.rest.client.connect-timeout}")
    private int connectTimeout;

    @Value("${spring.rest.client.max-per-route}")
    private int maxPerRoute;

    @Value("${spring.rest.client.max-total-connection}")
    private int maxTotalConnection;

    @Bean
    @Qualifier("dynamicConfigRestTemplate")
    public RestTemplate dynamicConfigRestTemplate() {
        return new RestTemplate(getClientHttpRequestFactory());
    }

    @Bean
    public HttpHeaders httpHeaders() {
        return new HttpHeaders();
    }

    @Bean
    public ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(getHttpClient());
        factory.setConnectTimeout(connectTimeout);
        factory.setReadTimeout(readTimeout);
        return factory;
    }

    @Bean
    public CloseableHttpClient getHttpClient() {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setDefaultMaxPerRoute(maxPerRoute);
        connectionManager.setMaxTotal(maxTotalConnection);
        return HttpClients.custom().setConnectionManager(connectionManager).build();
    }
}
