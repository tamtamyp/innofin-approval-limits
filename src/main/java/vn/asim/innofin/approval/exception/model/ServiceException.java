package vn.asim.innofin.approval.exception.model;

public class ServiceException extends AsimExceptions {

    private static final long serialVersionUID = 1L;

    public ServiceException(String message) {
        super.setMessage(message);
        super.setStatus("500");
    }
}
