package vn.asim.innofin.approval.exception.model;

public class NotExistException extends AsimExceptions {
    public NotExistException(String message) {
        super.setMessage(message);
        super.setStatus("400");
    }
}
