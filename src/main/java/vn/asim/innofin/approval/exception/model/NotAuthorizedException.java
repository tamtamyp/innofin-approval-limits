package vn.asim.innofin.approval.exception.model;

public class NotAuthorizedException extends AsimExceptions {

    private static final long serialVersionUID = 1L;

    public NotAuthorizedException(String message) {
        super.setMessage(message);
    }

    public NotAuthorizedException() {

    }
}
