package vn.asim.innofin.approval.exception.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthorizationException extends AsimExceptions {

    public AuthorizationException(String message) {
        super.setMessage(message);
        super.setStatus("401");
    }

}