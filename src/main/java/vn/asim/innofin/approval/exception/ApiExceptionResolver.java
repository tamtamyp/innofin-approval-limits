package vn.asim.innofin.approval.exception;

import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import vn.asim.innofin.approval.exception.model.BusinessException;
import vn.asim.innofin.approval.exception.model.ServiceException;

import javax.servlet.ServletException;
import java.util.function.Supplier;

@Component
public class ApiExceptionResolver {

    // Define service id in here
    private final String SERVICE_ID = "395";
    private final Supplier<String> unhandledExceptionCode = () -> SERVICE_ID + "999";
    private final Supplier<String> serviceExceptionCode = () -> SERVICE_ID + "998";
    private final Supplier<String> invalidInputFormatExceptionCode = () -> SERVICE_ID + "001";
    private final Supplier<String> apiInputExceptionCode = () -> SERVICE_ID + "002";


    public String resolve(Exception e) {
        // 400 status code
        if (e instanceof HttpMessageConversionException || e instanceof ServletException
                || e instanceof MethodArgumentNotValidException) {
            return invalidInputFormatExceptionCode.get();
        } else if (e instanceof ServiceException) {
            // 500 status code
            return serviceExceptionCode.get();
        } else if (e instanceof BusinessException) {
            return ((BusinessException) e).getErrorCode();
        } else {
            return unhandledExceptionCode.get();
        }
    }
}
