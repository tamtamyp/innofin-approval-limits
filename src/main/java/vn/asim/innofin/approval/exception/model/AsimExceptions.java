package vn.asim.innofin.approval.exception.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AsimExceptions extends RuntimeException {
    String message;
    String code;
    String status;
}
