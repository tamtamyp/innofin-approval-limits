package vn.asim.innofin.approval.exception.model;


public class DataConstrainException extends AsimExceptions{
    public DataConstrainException(String message) {
        super.setMessage(message);
        super.setStatus("400");
    }
}
