package vn.asim.innofin.approval.exception.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Value;

@Data
@EqualsAndHashCode(callSuper = false)
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String status;
    private String errorCode;
    private String errorMessage;

    @Value("${ixu.appcode}")
    private String applicationCode = "395";

    public BusinessException(String status, String errorCode, String errorMessage) {
        super(errorMessage);
        this.status = status;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public BusinessException(int errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = applicationCode.concat(String.valueOf(errorCode));
        this.errorMessage = errorMessage;
    }
}
