package vn.asim.innofin.approval.exception.model;

public class BadParameterException extends AsimExceptions {

    private static final long serialVersionUID = 1L;

    public BadParameterException(String message) {
        super.setMessage(message);
        super.setStatus("400");
    }
}