package vn.asim.innofin.approval.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import vn.asim.innofin.approval.constants.ResultMessageConstants;
import vn.asim.innofin.approval.dto.response.APIExceptionDto;
import vn.asim.innofin.approval.exception.model.*;

import javax.servlet.ServletException;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class ApiExceptionHandler {
    private final ApiExceptionResolver apiExceptionResolver;

    public ApiExceptionHandler(ApiExceptionResolver apiExceptionResolver) {
        this.apiExceptionResolver = apiExceptionResolver;
    }

    /**
     * 500 Internal server error
     */
    @ExceptionHandler({Exception.class,
            ServiceException.class,
            StorageException.class,
            StorageFileNotFoundException.class
    })
    protected ResponseEntity<Object> handleAll(Exception e) {
        log.error(e.getMessage(), e);
        APIExceptionDto dto = APIExceptionDto.builder()
                .status(HttpStatus.SC_INTERNAL_SERVER_ERROR + "")
                .code(apiExceptionResolver.resolve(e))
                .message(e.getMessage())
                .build();

        return ResponseEntity.status(HttpStatus.SC_INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }

    /**
     * 400 Bad Request
     */
    @ExceptionHandler({
            HttpMessageConversionException.class,
            ServletException.class,
            MethodArgumentNotValidException.class,
            IllegalArgumentException.class,
            MissingServletRequestParameterException.class,
            HttpMessageNotReadableException.class,
            MissingPathVariableException.class,
            TypeMismatchException.class,
            MethodArgumentTypeMismatchException.class,
            BadParameterException.class,
            ConstraintViolationException.class,
            DataConstrainException.class,
            NotExistException.class
    })
    public ResponseEntity<Object> handleAsimException(Exception e) {
    log.warn(e.getMessage(), e);
        String message = e.getMessage();
        if (e instanceof MethodArgumentNotValidException) {
            List<String> errorsList = new ArrayList<>();
            ((MethodArgumentNotValidException) e).getBindingResult().getAllErrors().forEach((error) -> {
                String fieldName = ((FieldError) error).getField();
                String errorMessage = error.getDefaultMessage();
                errorsList.add(String.format("%s: %s", fieldName, errorMessage));
            });
            message = StringUtils.join(errorsList.stream().sorted().collect(Collectors.toList()), ",");
        } else if (e instanceof IllegalArgumentException) {
            message = e.getMessage();
        } else if (e instanceof MethodArgumentTypeMismatchException) {
            message = ((MethodArgumentTypeMismatchException) e).getName() + ": bad parameter";
        } else if (e instanceof BusinessException) {
            message = ((BusinessException) e).getErrorMessage();
        } else if (e instanceof BadParameterException) {
            message = ((BadParameterException) e).getMessage();
        } else if (e instanceof HttpMessageNotReadableException) {
            if (e.getCause() != null) {
                message = e.getCause().getMessage();
            } else {
                message = ResultMessageConstants.REQUEST_BODY_IS_INVALID;
            }
        }
        APIExceptionDto dto = APIExceptionDto.builder()
                .status(HttpStatus.SC_BAD_REQUEST + "")
                .code(apiExceptionResolver.resolve(e))
                .message(message)
                .build();

        return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body(dto);
    }

    /**
     * 401 Unauthorized
     */
    @ExceptionHandler({AuthorizationException.class, NotAuthorizedException.class})
    public ResponseEntity<Object> handleMethodNotSupportedException(Exception e) {
        log.warn("AuthorizationException");
        String message = e.getMessage();
        APIExceptionDto dto = APIExceptionDto.builder()
                .status(HttpStatus.SC_UNAUTHORIZED + "")
                .code(apiExceptionResolver.resolve(e))
                .message(StringUtils.isBlank(message) ? "Unauthorized" : message)
                .build();

        return ResponseEntity.status(HttpStatus.SC_UNAUTHORIZED).contentType(MediaType.APPLICATION_JSON)
                .body(dto);
    }
}
