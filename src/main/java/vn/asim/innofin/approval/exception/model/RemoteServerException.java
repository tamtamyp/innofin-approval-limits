package vn.asim.innofin.approval.exception.model;

import lombok.Data;

@Data
public class RemoteServerException extends AsimExceptions{
    public RemoteServerException(String message) {
        super.setMessage(message);
        super.setStatus("500");
    }
}
