package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CollectorInfo {
    String id;
    String code;
    Date creationTime;
    Double limitPoint;
    Double creditScore;



}
