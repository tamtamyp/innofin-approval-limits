package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AppUserInfo {
    String phoneNumber;
    String userName;
    String name;
    String surname;

}
