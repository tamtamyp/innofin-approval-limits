package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CashInfo {
    String id;
    String collectionCode;
    Double total;
    Date requestTime;
    Date requestEndTime;
    Date processTime;
    int processStatus;

}
