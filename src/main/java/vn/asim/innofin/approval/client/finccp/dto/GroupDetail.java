package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GroupDetail {
    String id;
    Date creationTime;
    long numberOfLate;
    Long totalLimit;
    Long remainAmount;

    GroupCollectorInfo groupCollector;
}
