package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListDataGroupCollector {
    long totalCount;
    List<GroupDetail> items;
    @Data
    public class GroupDetail {
        String id;
        long numberOfLate;
        Long totalLimit;
        Long remainAmount;

        GroupCollectorInfo groupCollector;
    }
}
