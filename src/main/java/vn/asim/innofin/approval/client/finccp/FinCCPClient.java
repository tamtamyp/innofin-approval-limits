package vn.asim.innofin.approval.client.finccp;

import vn.asim.innofin.approval.client.finccp.dto.*;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.dto.response.DepositHistoryFinCPPDto;
import vn.asim.innofin.approval.entity.TenantProfile;

import java.util.List;

public interface FinCCPClient {
    ListDataCollector searchCollectorByName(TenantProfile tenantProfile, String name, long skipCount, long maxResultCount, String sortBy);

    CollectorDetail getCollectorById(TenantProfile tenantProfile, String id);

    ListDataGroupCollector searchGroupByName(TenantProfile tenantProfile, String name, long skipCount, long maxResultCount, String sortBy);

    GroupDetail getGroupById(TenantProfile tenantProfile, String id);

    ListData<CashCollectionDetail> getCashCollection(TenantProfile tenantProfile, String collectorId, LimitType limitType,
                                                    String code, String merchantId,
                                                     long skipCount, long maxResultCount, String sortBy);
    ListData<CashCollectionDetailv2> getCashCollectionv2(TenantProfile tenantProfile,String collectorId,  boolean isGroupLimit,  String filterCode,
                                                       String merchantId, long page, long size);
    boolean updateLimit(TenantProfile tenantProfile, String collectorId, LimitType limitType, Long amount);

    ListData<DepositHistoryFinCPPDto> getPaymentHistory(TenantProfile tenantProfile, String collectorId, boolean isGroup, long skipCount, long maxResultCount, String sortBy);

    boolean checkHold(TenantProfile tenantProfile, String collectorId, List<String> orderId, boolean flag);
}
