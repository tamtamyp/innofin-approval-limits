package vn.asim.innofin.approval.client.finccp;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Headers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import vn.asim.innofin.approval.client.finccp.dto.*;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.dto.request.HoldDto;
import vn.asim.innofin.approval.dto.request.NotifyDto;
import vn.asim.innofin.approval.dto.response.DepositHistoryFinCPPDto;
import vn.asim.innofin.approval.entity.TenantProfile;
import vn.asim.innofin.approval.exception.model.ServiceException;
import vn.asim.innofin.approval.utils.RequestUtils;
import vn.asim.innofin.approval.utils.RestUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FinCCPClientImpl implements FinCCPClient {

    private final RestTemplate restTemplate;

    @Value("${approval.clients.finccp.get-list-collector}")
    private String getListCollectorUrl;
    @Value("${approval.clients.finccp.get-details-collector}")
    private String getDetailCollectorUrl;
    @Value("${approval.clients.finccp.get-list-group}")
    private String getListGroupUrl;
    @Value("${approval.clients.finccp.get-details-group}")
    private String getDetailGroupUrl;

    @Value("${approval.clients.finccp.get-cash-collection}")
    private String getCashCollectionUrl;

    @Value("${approval.clients.finccp.get-cash-collection-v2}")
    private String getCashCollectionUrlV2;

    @Value("${approval.clients.finccp.update-limit-amount}")
    private String updateLimitUrl;

    @Value("/api/internal/deposit-histories")
    private String depositHistory;

    @Value("/api/internal/collector-unhold-cash-collection")
    private String unHold;

    @Value("/api/internal/collector-hold-cash-collection")
    private String hold;


    public FinCCPClientImpl(@Qualifier("dynamicConfigRestTemplate") RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    private static String className;
    static {
        FinCCPClientImpl.className = FinCCPClientImpl.class.getSimpleName();
    }
    @Override
    public ListDataCollector searchCollectorByName(TenantProfile tenantProfile, String name, long skipCount, long maxResultCount, String sortBy) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl()+getListCollectorUrl)
                .queryParam(FinCCPConstants.SearchCollectorParams.SKIP_COUNT, skipCount)
                .queryParam(FinCCPConstants.SearchCollectorParams.VERIFY, 1) // Default
                .queryParam(FinCCPConstants.SearchCollectorParams.TYPE_LIMIT, FinCCPConstants.TYPE_LIMIT.PRIVATE) // Default
                .queryParam(FinCCPConstants.SearchCollectorParams.MAX_RESULT_COUNT, maxResultCount);
        if(StringUtils.isNotBlank(name)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCollectorParams.FILTER_TEXT, name);
        }
        String uriString = uriBuilder.toUriString();
        log.info("Call GET : " + uriString);
        // HttpEntity<Void> httpEntity = new HttpEntity<>(RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            String methodTag = FinCCPClientImpl.className;
            Headers header = RestUtil.createHeaderForTenant(tenantProfile);
            ListDataCollector response = RequestUtils.getJsonHeader(methodTag, uriString, header, ListDataCollector.class);
            // ResponseEntity<ListData<CollectorDetail>> response = restTemplate.exchange(uriString, HttpMethod.GET, httpEntity,
            //         new ParameterizedTypeReference<ListData<CollectorDetail>>() {
            //         });
            
            return response;
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when get list collector: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when get list collector", e);
            }
            throw new ServiceException("Find Collector Error");
        }
    }

    @Override
    public ListData<DepositHistoryFinCPPDto> getPaymentHistory(TenantProfile tenantProfile, String collectorId, boolean isGroup, long skipCount, long maxResultCount, String sortBy) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl()+depositHistory)
                .queryParam(FinCCPConstants.SearchDepositHistory.COLLECTOR_ID, collectorId)
                .queryParam(FinCCPConstants.SearchDepositHistory.IS_GROUP_LIMIT, isGroup)
                .queryParam(FinCCPConstants.SearchDepositHistory.STATUS, FinCCPConstants.STATUS.DEPOSIT)
                .queryParam(FinCCPConstants.SearchDepositHistory.PAGE, skipCount)
                .queryParam(FinCCPConstants.SearchDepositHistory.SIZE, maxResultCount);
        String uriString = uriBuilder.toUriString();
        log.info("Call GET : " + uriString);
        HttpEntity<Void> httpEntity = new HttpEntity<>(RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            ResponseEntity<ListData<DepositHistoryFinCPPDto>> response = restTemplate.exchange(uriString, HttpMethod.GET, httpEntity,
                    new ParameterizedTypeReference<ListData<DepositHistoryFinCPPDto>>() {
                    });
            return response.getBody();
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when get list collector: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when get list collector", e);
            }
            throw new ServiceException("Find Collector Error");
        }
    }

    @Override
    public boolean checkHold(TenantProfile tenantProfile, String collectorId, List<String> orderId, boolean flag) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl() + (flag == true ? hold : unHold ));
        String uriString = uriBuilder.toUriString();
        HoldDto holdRequest = HoldDto.builder()
                .user(collectorId)
                .cashCollectionId(orderId)
                .build();
        log.info("Call POST : " + uriString );
        HttpEntity<HoldDto> httpEntity = new HttpEntity<>(holdRequest, RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            ResponseEntity<String> response = restTemplate.exchange(uriString, HttpMethod.POST, httpEntity,
                    new ParameterizedTypeReference<String>() {
                    });
            log.info(response.getBody());
            return Boolean.parseBoolean(response.getBody());
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when call hold: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when call hold", e);
            }
            throw new ServiceException("Error");
        }
    }

    @Override
    public CollectorDetail getCollectorById(TenantProfile tenantProfile, String id) {
        Map<String, String> requestParamsMap = new HashMap<>();
        requestParamsMap.put("collector-id", id);
        String uriString = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl() + getDetailCollectorUrl)
                .buildAndExpand(requestParamsMap)
                .toUriString();
        log.info("Call GET: " + uriString);
        HttpEntity<Void> httpEntity = new HttpEntity<>(RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            ResponseEntity<CollectorDetail> response = restTemplate.exchange(uriString, HttpMethod.GET, httpEntity,
                    new ParameterizedTypeReference<CollectorDetail>() {
                    });
            return response.getBody();
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when get collector detail: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when get collector detail", e);
            }
            //throw new ServiceException("Get Collector Detail Error");
            return null;
        }
    }

    @Override
    public ListDataGroupCollector searchGroupByName(TenantProfile tenantProfile, String name, long skipCount, long maxResultCount, String sortBy) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl() + this.getListGroupUrl)
                .queryParam(FinCCPConstants.SearchGroupParams.SKIP_COUNT, skipCount)
                .queryParam(FinCCPConstants.SearchGroupParams.MAX_RESULT_COUNT, maxResultCount)
                .queryParam(FinCCPConstants.SearchGroupParams.SORTING, sortBy);
        if(StringUtils.isNotBlank(name)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchGroupParams.FILTER_TEXT, name);
        }
        String uriString = uriBuilder.toUriString();
        log.info("Call GET : " + uriString);
        HttpEntity<Void> httpEntity = new HttpEntity<>(RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            String methodTag = FinCCPClientImpl.className;
            Headers header = RestUtil.createHeaderForTenant(tenantProfile);
            ListDataGroupCollector response = RequestUtils.getJsonHeader(methodTag, uriString, header, ListDataGroupCollector.class);

            // ResponseEntity<ListData<GroupDetail>> response = restTemplate.exchange(uriString, HttpMethod.GET, httpEntity,
            //         new ParameterizedTypeReference<ListData<GroupDetail>>() {
            //         });
            return response;
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when get list group: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when get list group", e);
            }
            throw new ServiceException("Find Group Error");
        }
    }

    @Override
    public GroupDetail getGroupById(TenantProfile tenantProfile, String id) {
        Map<String, String> requestParamsMap = new HashMap<>();
        requestParamsMap.put("group-id", id);
        String uriString = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl() + getDetailGroupUrl)
                .buildAndExpand(requestParamsMap)
                .toUriString();
        log.info("Call GET: " + uriString);
        HttpEntity<Void> httpEntity = new HttpEntity<>(RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            ResponseEntity<GroupDetail> response = restTemplate.exchange(uriString, HttpMethod.GET, httpEntity,
                    new ParameterizedTypeReference<GroupDetail>() {
                    });
            return response.getBody();
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when get group detail: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when get group detail", e);
            }
            //throw new ServiceException("Get Collector Detail Error");
            return null;
        }
    }

    @Override
    public ListData<CashCollectionDetail> getCashCollection(TenantProfile tenantProfile,
                                                            String collectorId,
                                                            LimitType limitType,
                                                            String code,
                                                            String merchantId,
                                                            long skipCount, long maxResultCount, String sortBy) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl() + this.getCashCollectionUrl)
                .queryParam(FinCCPConstants.SearchCashCollectionParams.SKIP_COUNT, skipCount)
                .queryParam(FinCCPConstants.SearchCashCollectionParams.MAX_RESULT_COUNT, maxResultCount);
//                .queryParam(FinCCPConstants.SearchCashCollectionParams.SearchGroupParams.SORTING, sortBy);
        if(StringUtils.isNotBlank(collectorId)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCashCollectionParams.COLLECTOR_ID, collectorId);
        }
        if(StringUtils.isNotBlank(code)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCashCollectionParams.COLLECTION_CODE, code);
        }
        if(StringUtils.isNotBlank(merchantId)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCashCollectionParams.MERCHANT_ID, merchantId);
        }

        String uriString = uriBuilder.toUriString();
        log.info("Call GET : " + uriString);
        HttpEntity<Void> httpEntity = new HttpEntity<>(RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            ResponseEntity<ListData<CashCollectionDetail>> response = restTemplate.exchange(uriString, HttpMethod.GET, httpEntity,
                    new ParameterizedTypeReference<ListData<CashCollectionDetail>>() {
                    });
            return response.getBody();
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when get list cash collection: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when get list cash collection", e);
            }
            throw new ServiceException("Find Cash Collection Error");
        }
    }

    @Override
    public ListData<CashCollectionDetailv2> getCashCollectionv2(TenantProfile tenantProfile, String collectorId, boolean isGroupLimit, String filterCode,
                                                                String merchantId, long page, long size){
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(tenantProfile.getBaseUrl() + this.getCashCollectionUrlV2)
                .queryParam(FinCCPConstants.SearchCashCollectionV2Params.PAGE, page)
                .queryParam(FinCCPConstants.SearchCashCollectionV2Params.SIZE, size);

        if(StringUtils.isNotBlank(collectorId)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCashCollectionV2Params.COLLECTOR_ID, collectorId);
        }
        if(StringUtils.isNotBlank(filterCode)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCashCollectionV2Params.FILTER_CODE, filterCode);
        }
        if(StringUtils.isNotBlank(merchantId)){
            uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCashCollectionV2Params.MERCHANT_ID, merchantId);
        }

        uriBuilder = uriBuilder.queryParam(FinCCPConstants.SearchCashCollectionV2Params.IS_GROUP_LIMIT, isGroupLimit);

        String uriString = uriBuilder.toUriString();
        log.info("Call GET : " + uriString);
        HttpEntity<ListData<CashCollectionDetailv2>> httpEntity = new HttpEntity<>(RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            ResponseEntity<ListData<CashCollectionDetailv2>> response = restTemplate.exchange(uriString, HttpMethod.GET, httpEntity,
                    new ParameterizedTypeReference<ListData<CashCollectionDetailv2>>() {
                    });
            return response.getBody();
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when get list cash collection: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when get list cash collection", e);
            }
            throw new ServiceException("Truy vấn dữ liệu hệ thống "+ tenantProfile.getName()+" lỗi");
        }
    }

    @Override
    public boolean updateLimit(TenantProfile tenantProfile, String collectorId, LimitType limitType, Long amount) {
        String uriString = tenantProfile.getBaseUrl() + this.updateLimitUrl;

        UpdateLimitRequest updateLimitRequest = UpdateLimitRequest.builder()
                .id(collectorId)
                .limitAmount(amount)
                .typeLimit(LimitType.GROUP.equals(limitType)?1:0)
                .build();
        log.info("Call POST : " + uriString );
        log.info("Request Body: {}", updateLimitRequest);
        HttpEntity<UpdateLimitRequest> httpEntity = new HttpEntity<>(updateLimitRequest, RestUtil.createHttpHeaderForTenant(tenantProfile));
        try {
            ResponseEntity<String> response = restTemplate.exchange(uriString, HttpMethod.POST, httpEntity,
                    new ParameterizedTypeReference<String>() {
                    });
            log.info(response.getBody());
            return Boolean.parseBoolean(response.getBody());
        } catch (Exception e) {
            log.error(e.getMessage());
            if(e instanceof HttpClientErrorException){
                log.info("Received response status from client [{}]",
                        ((HttpClientErrorException)e).getResponseBodyAsString());
            }else if(e instanceof HttpServerErrorException){
                log.error("Error occurred when update limit: {}",
                        ((HttpServerErrorException)e).getResponseBodyAsString(), e);
                String responseStr = ((HttpServerErrorException)e).getResponseBodyAsString();
                Gson gson = new Gson();
                FinCcpExceptionResult exceptionResult = gson.fromJson(responseStr, FinCcpExceptionResult.class);
                throw new ServiceException(exceptionResult.getError().getMessage());
            }else{
                log.error("Error occurred when update limit", e);
            }
            throw new ServiceException("Chuyển tiếp yêu cầu đã được phê duyệt sang "+tenantProfile.getName()+" Lỗi");
        }
    }
}
