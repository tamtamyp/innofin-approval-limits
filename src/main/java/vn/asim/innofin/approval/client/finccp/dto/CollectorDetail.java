package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CollectorDetail {
    CollectorInfo collector;
    AppUserInfo appUser;
    WalletInfo wallet;
    Long totalLimitOU;
    Long totalCurrentAmountOU;
    Long remaininglimitOU;
    Long numberOfLate;
}
