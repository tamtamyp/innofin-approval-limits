package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class FinCcpExceptionResult {
    FinCcpException error;
}
