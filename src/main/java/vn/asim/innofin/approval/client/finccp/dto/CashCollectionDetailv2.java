package vn.asim.innofin.approval.client.finccp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CashCollectionDetailv2 {
    String id;
    String code;
    String storeName;
    String merchantName;
    Date requestDateTime;
    Date requestEndTime;
    int processStatus;
    Long total;
    String note;
    BankInfo bank;
    Long amountRecover; // số tiền thu hồi
    Long amountAllocate; // số tiền cấp phát
}
