package vn.asim.innofin.approval.client.finccp;

public class FinCCPConstants {
    public class SearchCollectorParams {
        public static final String FILTER_TEXT = "FilterText";
        public static final String SKIP_COUNT = "SkipCount";
        public static final String MAX_RESULT_COUNT = "MaxResultCount";
        public static final String VERIFY = "Verify";
        public static final String TYPE_LIMIT = "TypeLimit";
        public static final String SORTING = "Sorting";
    }

    public class SearchGroupParams {
        public static final String FILTER_TEXT = "FilterText";
        public static final String SKIP_COUNT = "SkipCount";
        public static final String MAX_RESULT_COUNT = "MaxResultCount";
        public static final String SORTING = "Sorting";
    }

    public class SearchCashCollectionParams {
        public static final String FILTER_TEXT = "FilterText";
        public static final String SKIP_COUNT = "SkipCount";
        public static final String MAX_RESULT_COUNT = "MaxResultCount";
        public static final String SORTING = "Sorting";
        public static final String COLLECTOR_ID = "CollectorId";
        public static final String COLLECTION_CODE = "CollectionCode";
        public static final String MERCHANT_ID = "MerchantId";
    }

    public class SearchCashCollectionV2Params {
        public static final String FILTER_CODE = "FilterCode";
        public static final String COLLECTOR_ID = "CollectorId";
        public static final String MERCHANT_ID = "MerchantId";
        public static final String IS_GROUP_LIMIT = "isGroupLimit";
        public static final String PAGE = "Page";
        public static final String SIZE = "Size";

        public static final String SORTING = "Sorting";
    }

    public class TYPE_LIMIT {
        public static final int PRIVATE = 0;
        public static final int PUBLIC = 1;
    }

    public class SearchDepositHistory {
        public static final String IS_GROUP_LIMIT = "IsGroupLimit";
        public static final String COLLECTOR_ID = "CollectorId";
        public static final String STATUS = "Status";
        public static final String PAGE = "Page";
        public static final String SIZE = "Size";
        public static final String SORT = "Sort";
    }

    public class STATUS {
        public static final int DEPOSIT = 1;
    }
}
