package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.response.RequestLimitRes;
import vn.asim.innofin.approval.entity.RequestLimit;

@Repository
@Mapper(componentModel = "spring")
public interface RequestLimitMapper extends iBaseMapper<RequestLimitRes, RequestLimit> {

    @Override
    RequestLimit toEntity(RequestLimitRes dto);

    @Override
    RequestLimitRes toDto(RequestLimit entity);


}
