package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.client.finccp.dto.CashCollectionDetailv2;
import vn.asim.innofin.approval.dto.response.CashCollectionRes;
import vn.asim.innofin.approval.entity.RequestLimitForOrder;

@Repository
@Mapper(componentModel = "spring")
public interface RequestLimitOrderMapper extends iBaseMapper<CashCollectionRes, RequestLimitForOrder> {
    @Override
    RequestLimitForOrder toEntity(CashCollectionRes dto);

    @Override
    CashCollectionRes toDto(RequestLimitForOrder entity);

    @Mapping(source ="detailv2.total", target = "amount")
    @Mapping(source ="detailv2.bank.name", target = "bankName")
    @Mapping(source ="detailv2.amountRecover", target = "amountRecover")
    @Mapping(source ="detailv2.amountAllocate", target = "amountAllocate")
    CashCollectionRes fromFinCppDto(CashCollectionDetailv2 detailv2);
}

