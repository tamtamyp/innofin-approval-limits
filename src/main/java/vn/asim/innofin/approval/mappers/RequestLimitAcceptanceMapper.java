package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.response.RequestLimitLevelAcceptanceRes;
import vn.asim.innofin.approval.entity.RequestLimitAcceptance;

@Repository
@Mapper(componentModel = "spring")
public interface RequestLimitAcceptanceMapper extends iBaseMapper<RequestLimitLevelAcceptanceRes, RequestLimitAcceptance> {

    @Override
    RequestLimitAcceptance toEntity(RequestLimitLevelAcceptanceRes dto);

    @Override
    RequestLimitLevelAcceptanceRes toDto(RequestLimitAcceptance entity);


    /**
     * @Mapping(source = "entity.name", target = "contractName")
     *   @Named("toSingleDto")
     *   ContractSearchResDto toSearchResDto(Contract entity);
     *
     *   @Named("toSingleDto")
     *   @Mapping(source = "entity.name",target = "contractName")
     *   ContractResDto toResDto(Contract entity);
     *
     *   @Mapping(source = "entity.name", target = "collateralManagementName")
     *   @Named("toSingleDto")
     *   ContractSimpleDto toContractSimpleDto(Contract entity);
     *
     *   @Named("toSingleDto")
     *   ImplementContractDto toImplementContractDto(Contract entity);
     */
}