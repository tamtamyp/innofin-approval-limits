package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.response.ApprovalHistoryResDto;
import vn.asim.innofin.approval.entity.RequestLimitAcceptance;

@Repository
@Mapper(componentModel = "spring")
public interface ApprovalHistoryMapper extends iBaseMapper<ApprovalHistoryResDto, RequestLimitAcceptance> {
    @Override
    RequestLimitAcceptance toEntity(ApprovalHistoryResDto dto);

    @Override
    ApprovalHistoryResDto toDto(RequestLimitAcceptance entity);

}

