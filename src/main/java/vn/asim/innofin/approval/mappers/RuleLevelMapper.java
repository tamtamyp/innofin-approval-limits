package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.response.RuleLevelDto;
import vn.asim.innofin.approval.entity.RuleLevel;

@Repository
@Mapper(componentModel = "spring")
public interface RuleLevelMapper extends iBaseMapper<RuleLevelDto, RuleLevel> {
    @Override
    RuleLevel toEntity(RuleLevelDto dto);

    @Override
    RuleLevelDto toDto(RuleLevel entity);

}
