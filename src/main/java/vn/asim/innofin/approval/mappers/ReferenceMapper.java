package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.ReferenceDataDto;
import vn.asim.innofin.approval.entity.ReferenceData;

@Repository
@Mapper(componentModel = "spring")
public interface ReferenceMapper extends iBaseMapper<ReferenceDataDto, ReferenceData> {
    @Override
    ReferenceData toEntity(ReferenceDataDto dto);

    @Override
    ReferenceDataDto toDto(ReferenceData entity);

}