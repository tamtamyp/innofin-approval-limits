package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.response.FileResDto;
import vn.asim.innofin.approval.entity.FileData;

@Repository
@Mapper(componentModel = "spring")
public interface FileDataMapper extends iBaseMapper<FileResDto, FileData> {
    @Override
    FileData toEntity(FileResDto dto);

    @Override
    FileResDto toDto(FileData entity);

}
