package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.RuleDto;
import vn.asim.innofin.approval.entity.Rule;

@Repository
@Mapper(componentModel = "spring")
public interface RuleMapper extends iBaseMapper<RuleDto, Rule> {
    @Override
    Rule toEntity(RuleDto dto);

    @Override
    RuleDto toDto(Rule entity);

}
