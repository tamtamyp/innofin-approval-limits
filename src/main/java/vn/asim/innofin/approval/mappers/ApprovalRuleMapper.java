package vn.asim.innofin.approval.mappers;

import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;
import vn.asim.innofin.approval.dto.response.ApprovalRuleResDto;
import vn.asim.innofin.approval.entity.Rule;

@Repository
@Mapper(componentModel = "spring")
public interface ApprovalRuleMapper extends iBaseMapper<ApprovalRuleResDto, Rule> {
    @Override
    Rule toEntity(ApprovalRuleResDto dto);

    @Override
    ApprovalRuleResDto toDto(Rule entity);

}

