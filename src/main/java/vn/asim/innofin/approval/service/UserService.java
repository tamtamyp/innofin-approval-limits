package vn.asim.innofin.approval.service;

import vn.asim.innofin.approval.config.jwt.JwtInfo;

public interface UserService {
    public String login(String username, String password);

    public void logout(JwtInfo jwtInfo);
}
