package vn.asim.innofin.approval.service;

import vn.asim.innofin.approval.dto.response.FileDto;

import java.io.IOException;

public interface FileDataService {
    public String saveFile(Long requestLimitId, String fileName, byte[] fileData);

    public FileDto getFile(String code) throws IOException;

    public void deleteFileOfRequestLimit(Long requestLimit);

    public void deleteFileOfRequestLimitAndFileCode(Long requestLimitId, String fileCode);
}
