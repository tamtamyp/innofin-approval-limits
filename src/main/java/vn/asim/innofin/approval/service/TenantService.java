package vn.asim.innofin.approval.service;

import vn.asim.innofin.approval.dto.response.TenantResDto;

import java.util.List;

public interface TenantService {
    List<TenantResDto> getAllTenants();
}
