package vn.asim.innofin.approval.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import vn.asim.innofin.approval.client.finccp.FinCCPClient;
import vn.asim.innofin.approval.client.finccp.dto.CollectorDetail;
import vn.asim.innofin.approval.client.finccp.dto.GroupDetail;
import vn.asim.innofin.approval.client.finccp.dto.ListData;
import vn.asim.innofin.approval.client.finccp.dto.ListDataCollector;
import vn.asim.innofin.approval.client.finccp.dto.ListDataGroupCollector;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.Paginate;
import vn.asim.innofin.approval.dto.response.CollectorResDto;
import vn.asim.innofin.approval.dto.response.GroupCollectorResDto;
import vn.asim.innofin.approval.entity.TenantProfile;
import vn.asim.innofin.approval.exception.model.DataConstrainException;
import vn.asim.innofin.approval.service.CollectorService;
import vn.asim.innofin.approval.service.CommonService;
import vn.asim.innofin.approval.utils.VNCharacterUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class CollectorsServiceImpl implements CollectorService {

    private final CommonService commonService;
    private final FinCCPClient finCCPClient;

    @Override
    public BaseListResponse<CollectorResDto> searchCollector(String requestSystem, String name, int page, int size) {
        TenantProfile tenantProfile = commonService.verifyTenant(requestSystem);
        //Get Info from tenant
        long skipCount = page * size;
//        name = VNCharacterUtils.removeAccent(name).trim();
        ListDataCollector rawDetailFromTenant = finCCPClient.searchCollectorByName(tenantProfile, name, skipCount, size, "Code");
        List<CollectorResDto> resultContent = rawDetailFromTenant.getItems().stream().map(c -> {
                    Double currentLimit = c.getCollector() != null ? c.getCollector().getLimitPoint() : null;
                    Double remainLimit = c.getWallet() != null ? c.getWallet().getPoint() : null;
                    if (currentLimit == null || remainLimit == null) {
                        if (currentLimit == null) {
                            log.error("Collector or current limit of collector is null");
                        }
                        if (remainLimit == null) {
                            log.error("Wallet or point of wallet is null");
                        }
                        throw new DataConstrainException("Data from FinCCP Error");
                    }
                    return CollectorResDto.builder()
                            .id(c.getCollector().getId())
                            .currentLimit(commonService.convertPointToMoney(currentLimit))
                            //TODO Fix. current mapping value is not true
                            .warningCount(c.getNumberOfLate())
                            .startWorkingDate(c.getCollector().getCreationTime())
                            .name(c.getAppUser().getSurname() + " " + c.getAppUser().getName())
                            .remainAmount(commonService.convertPointToMoney((remainLimit.doubleValue())))
                            .build();
                }
        ).collect(Collectors.toList());
        BaseListResponse<CollectorResDto> baseListResponse = new BaseListResponse<>();
        baseListResponse.setData(resultContent);
        baseListResponse.setPaginate(Paginate.builder()
                        .totalElement(rawDetailFromTenant.getTotalCount())
                        .totalPage((long) Math.ceil(rawDetailFromTenant.getTotalCount()/(double)size))
                        .size(size)
                        .page(page)
                        .build());
        return baseListResponse;
    }


    @Override
    public BaseListResponse<GroupCollectorResDto> searchCollectorGroup(String requestSystem, String name, int page, int size) {
        TenantProfile tenantProfile = commonService.verifyTenant(requestSystem);
        //Get Info from tenant
        long skipCount = page * size;
        // name = VNCharacterUtils.removeAccent(name).trim();
        ListDataGroupCollector rawDetailFromTenant = finCCPClient.searchGroupByName(tenantProfile, name, skipCount, size, "Name");
        List<GroupCollectorResDto> resultContent = rawDetailFromTenant.getItems().stream()
                .map(g -> GroupCollectorResDto.builder()
                        .id(g.getGroupCollector().getId())
                        .currentLimit(g.getTotalLimit())
                        .warningCount(g.getNumberOfLate())
                        .startWorkingDate(g.getGroupCollector().getCreationTime())
                        .name(g.getGroupCollector().getName())
                        .remainAmount(g.getRemainAmount())
                        .build()
        ).collect(Collectors.toList());
        BaseListResponse<GroupCollectorResDto> baseListResponse = new BaseListResponse<>();
        baseListResponse.setData(resultContent);
        baseListResponse.setPaginate(Paginate.builder()
                .totalElement(rawDetailFromTenant.getTotalCount())
                .totalPage((long) Math.ceil(rawDetailFromTenant.getTotalCount()/(double)size))
                .size(size)
                .page(page)
                .build());
        return baseListResponse;
    }
}
