package vn.asim.innofin.approval.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import vn.asim.innofin.approval.service.EmailService;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final SpringTemplateEngine thymeleafTemplateEngine;

    private final JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String sender;

    @Value("${approval.content.email.request-limit.title}")
    private String requestLimitTitle;
    @Value("${approval.content.email.request-limit.template}")
    private String requestLimitTemplate;
    @Value("${approval.content.email.approved.title}")
    private String approvedTitle;
    @Value("${approval.content.email.approved.template}")
    private String approvedTemplate;
    @Value("${approval.content.email.declined.title}")
    private String declinedTitle;
    @Value("${approval.content.email.declined.template}")
    private String declinedTemplate;
    @Value("${approval.content.email.ref-link}")
    private String refLink;

    private void send(String[] to, String[] cc, String[] bcc, String title, String content){
        try{

            MimeMessage message = mailSender.createMimeMessage();
            final MimeMessageHelper helper =
                    new MimeMessageHelper(message, false, "UTF-8");
            // Setting up necessary details
            helper.setFrom(sender);
            helper.setTo(to);
            if(cc != null && cc.length > 0){
                helper.setCc(cc);
            }
            if(bcc != null && bcc.length > 0){
                helper.setBcc(bcc);
            }
            helper.setText(content, true);
            helper.setSubject(title);
            // Sending the mail
            mailSender.send(message);
            log.info("Sent");
        }catch (Exception e){
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void sendRequestApprovalEmail(String[] to, String[] cc, String[] bcc, Map<String, Object> variables) {
        Context thymeleafContext = new Context();
        if(variables == null){
            variables = new HashMap<>();
        }
        variables.put("refLink",refLink);
        thymeleafContext.setVariables(variables);
        String htmlBody = thymeleafTemplateEngine.process(requestLimitTemplate, thymeleafContext);
        send(to,cc,bcc, requestLimitTitle, htmlBody);
    }

    @Override
    public void sendApprovedEmail(String[] to, String[] cc, String[] bcc, Map<String, Object> variables) {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(variables);
        String htmlBody = thymeleafTemplateEngine.process(approvedTemplate, thymeleafContext);
        send(to,cc,bcc, approvedTitle, htmlBody);
    }

    @Override
    public void sendDeclinedEmail(String[] to, String[] cc, String[] bcc, Map<String, Object> variables) {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(variables);
        String htmlBody = thymeleafTemplateEngine.process(declinedTemplate, thymeleafContext);
        send(to,cc,bcc, declinedTitle, htmlBody);
    }
}
