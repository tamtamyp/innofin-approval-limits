package vn.asim.innofin.approval.service;

import org.springframework.data.domain.Page;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.constants.enums.RequestLimitStatus;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.request.CreateRqApprovalLimitDto;
import vn.asim.innofin.approval.dto.request.RqApproveDto;
import vn.asim.innofin.approval.dto.request.UpdateRqApprovalLimitDto;
import vn.asim.innofin.approval.dto.response.ApprovalHistoryResDto;
import vn.asim.innofin.approval.dto.response.DepositHistoryDto;
import vn.asim.innofin.approval.dto.response.RequestLimitRes;
import vn.asim.innofin.approval.entity.RequestLimitForOrder;

import java.io.InputStream;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public interface RequestLimitService {

    Page<RequestLimitRes> advancedSearch(
            List<String> requestSystems,
            LimitType limitType,
            List<String> collectorId,
            List<RequestLimitStatus> statusList,
            Date createdDateFrom,
            Date createdDateTo,
            BigInteger limitFrom,
            BigInteger limitTo,
            int page,
            int size);

    RequestLimitRes getDetails(Long id);

    RequestLimitRes create(JwtInfo jwtInfo, CreateRqApprovalLimitDto requestApprovalLimits);
    RequestLimitRes update(JwtInfo jwtInfo, Long requestLimitId, UpdateRqApprovalLimitDto requestApprovalLimits);

    void deleteBatch(List<Long> ids);

    InputStream exportToExcel(List<String> requestSystems,
                              LimitType limitType,
                              List<String> collectorId,
                              List<RequestLimitStatus> statusList,
                              Date createdDateFrom,
                              Date createdDateTo,
                              BigInteger limitFrom,
                              BigInteger limitTo);

    void approve(JwtInfo jwtInfo, RqApproveDto requestApprovalLimits);

    Page<ApprovalHistoryResDto> getApproveHistory(LimitType limitType, String objectId, int page, int size);

    BaseListResponse<DepositHistoryDto> getPaymentHistory(String requestSystem, String collectorId, LimitType limitType, Integer page, Integer size);
    List<Long> getAllocateAmount(String requestSystem, LimitType limitType, String collectorId, List<Double> ordersId);
}
