package vn.asim.innofin.approval.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import vn.asim.innofin.approval.config.annotation.NoLogging;
import vn.asim.innofin.approval.constants.enums.CashCollectionProcessStatus;
import vn.asim.innofin.approval.constants.enums.ReferenceDataType;
import vn.asim.innofin.approval.dto.ReferenceDataDto;
import vn.asim.innofin.approval.entity.ReferenceData;
import vn.asim.innofin.approval.entity.TenantProfile;
import vn.asim.innofin.approval.exception.model.BadParameterException;
import vn.asim.innofin.approval.mappers.ReferenceMapper;
import vn.asim.innofin.approval.repo.ReferenceDataRepo;
import vn.asim.innofin.approval.repo.TenantRepo;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommonService {

    @Value("${approval.pointConvert:1000}")
    public long pointConvert;

    private final TenantRepo tenantRepo;

    private final ReferenceDataRepo referenceDataRepo;
    final Map<String, List<ReferenceData>> mapData = new HashMap<>();

    private final ReferenceMapper refMapper;

    //Key = type+@+code
    final Map<String, ReferenceData> mapKey = new HashMap<>();
    @PostConstruct
    public void initialStaticData(){
        Iterable<ReferenceData> refIterable = referenceDataRepo.findAll(Sort.by(Sort.Direction.ASC,"orderNumber"));
        for(ReferenceData referenceData : refIterable){
            List<ReferenceData> list = mapData.get(referenceData.getType());
            if(list == null){
                list = new ArrayList<>();
                mapData.put(referenceData.getType(), list);
            }
            list.add(referenceData);
            mapKey.put(referenceData.getType()+"@"+referenceData.getCode(), referenceData);
        }
    }

    public TenantProfile verifyTenant(String code) {
        TenantProfile tenant = tenantRepo.findByCodeWithCache(code);
        if (tenant == null) {
            log.warn("Tenant {} is not exist", code);
            throw new BadParameterException("System Request is not exist");
        }
        return tenant;
    }

    public TenantProfile findTenantByCode(String code){
        return tenantRepo.findByCodeWithCache(code);
    }

    public String getRequestStatusNameByCode(String code){
        ReferenceData ref = mapKey.get(ReferenceDataType.REQUEST_STATUS.getName()+"@"+code);
        if(ref == null){
            log.warn("Request status with code {} is not found", code);
            return null;
        }
        return ref.getName();
    }

    public String getRoleNameByCode(String code){
        ReferenceData ref = mapKey.get(ReferenceDataType.ROLES.getName()+"@"+code);
        if(ref == null){
            log.warn("Role with code {} is not found", code);
            return null;
        }
        return ref.getName();
    }

    public String getLimitTypeNameByCode(String code){
        ReferenceData ref = mapKey.get(ReferenceDataType.LIMIT_TYPE.getName()+"@"+code);
        if(ref == null){
            log.warn("Limit type with code {} is not found", code);
            return null;
        }
        return ref.getName();
    }

    public List<ReferenceDataDto> getRef(ReferenceDataType referenceDataType){
        List<ReferenceData> rawResult = mapData.get(referenceDataType.getName());
        return rawResult.stream().map(r->refMapper.toDto(r)).collect(Collectors.toList());
    }

    private final Map<Integer, CashCollectionProcessStatus> mapCashStatus = new HashMap<>();
    @PostConstruct
    private void initMapCashStatus(){
        for(CashCollectionProcessStatus s : CashCollectionProcessStatus.values()){
            mapCashStatus.put(s.getCode(), s);
        }
    }

    @NoLogging
    public CashCollectionProcessStatus getCashCollectionStatusFromStatusId(int statusId){
        return mapCashStatus.get(statusId);
    }

    @NoLogging
    public Long convertPointToMoney(double point){
        return (long) (point * pointConvert);
    }
}
