package vn.asim.innofin.approval.service;

import org.springframework.data.domain.Page;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.response.CollectorResDto;
import vn.asim.innofin.approval.dto.response.GroupCollectorResDto;

public interface CollectorService {
    public BaseListResponse<CollectorResDto> searchCollector(String requestSystem, String name, int page, int size);

    BaseListResponse<GroupCollectorResDto> searchCollectorGroup(String requestSystem, String name, int page, int size);
}
