package vn.asim.innofin.approval.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import vn.asim.innofin.approval.dto.response.FileDto;
import vn.asim.innofin.approval.entity.FileData;
import vn.asim.innofin.approval.repo.FileDataRepo;
import vn.asim.innofin.approval.service.FileDataService;
import vn.asim.innofin.approval.utils.FileUtils;

import java.io.IOException;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class FileDataServiceImpl implements FileDataService {

    private final FileDataRepo fileDataRepo;
    @Override
    public String saveFile(Long requestLimitId, String fileName, byte[] fileData) {
        String fileCode = UUID.randomUUID().toString();
        FileUtils.save(fileCode, fileData);
        FileData f = FileData.builder().fileCode(fileCode).fileName(fileName).requestLimitId(requestLimitId).build();
        fileDataRepo.save(f);
        return fileCode;
    }

    @Override
    public FileDto getFile(String code) throws IOException {
        FileData fileData = fileDataRepo.findFileDataByFileCode(code);
        if(fileData == null){
            log.warn("File {} is not found",code);
            throw new NotFoundException("File is not found");
        }
        Resource resource = FileUtils.loadAsResource(code);
        return FileDto.builder()
                .fileName(fileData.getFileName())
                .fileData(new InputStreamResource(resource.getInputStream()))
                .build();
    }

    @Override
    public void deleteFileOfRequestLimit(Long requestLimit){
        fileDataRepo.deleteFileDataByRequestLimitId(requestLimit);
    }

    @Override
    public void deleteFileOfRequestLimitAndFileCode(Long requestLimitId, String fileCode){
        fileDataRepo.deleteFileDataByRequestLimitIdAndFileCode(requestLimitId, fileCode);
    }
}
