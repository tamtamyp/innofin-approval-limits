package vn.asim.innofin.approval.service;

import org.springframework.data.domain.Page;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.dto.RuleDto;
import vn.asim.innofin.approval.dto.response.MatchingRuleRes;

import java.io.InputStream;
import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.List;

public interface RuleService {
    MatchingRuleRes findMatchingRule(LimitType limitType, Long requestTotalValue);

    Page<RuleDto> search(String name, OffsetDateTime effectiveFrom, OffsetDateTime effectiveTo, int page, int size);

    InputStream exportToExcel(String name, OffsetDateTime effectiveFrom, OffsetDateTime effectiveTo);

    boolean deleteBatch(List<String> ruleIds);
}
