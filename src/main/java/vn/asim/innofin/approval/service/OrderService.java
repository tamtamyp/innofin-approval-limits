package vn.asim.innofin.approval.service;

import org.springframework.data.domain.Page;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.response.CashCollectionRes;
import vn.asim.innofin.approval.dto.response.MerchantDto;

import java.util.List;

public interface OrderService {
    BaseListResponse<CashCollectionRes> getOrders(String requestSystem, String collectorId, LimitType limitType, String code, String merchantId, int page, int size);

    List<MerchantDto> getAllMerchants(String requestSystem);
}
