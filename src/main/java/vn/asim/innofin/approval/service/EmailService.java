package vn.asim.innofin.approval.service;

import vn.asim.innofin.approval.dto.EmailDetails;

import java.util.Map;

public interface EmailService {
    void sendRequestApprovalEmail(String[] to, String[] bbc, String[] cc, Map<String, Object> variables);
    void sendApprovedEmail(String[] to, String[] cc, String[] bcc, Map<String, Object> variables);
    void sendDeclinedEmail(String[] to, String[] cc, String[] bcc, Map<String, Object> variables);
}
