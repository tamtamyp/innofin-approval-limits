package vn.asim.innofin.approval.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.dto.RuleDto;
import vn.asim.innofin.approval.dto.response.MatchingRuleRes;
import vn.asim.innofin.approval.dto.response.RuleLevelDto;
import vn.asim.innofin.approval.entity.Rule;
import vn.asim.innofin.approval.entity.RuleLevel;
import vn.asim.innofin.approval.mappers.RuleLevelMapper;
import vn.asim.innofin.approval.mappers.RuleMapper;
import vn.asim.innofin.approval.repo.RuleLevelRepo;
import vn.asim.innofin.approval.repo.RuleRepo;
import vn.asim.innofin.approval.service.RuleService;
import vn.asim.innofin.approval.utils.VNCharacterUtils;

import java.io.InputStream;
import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class RuleServiceImpl implements RuleService {

    private final RuleRepo ruleRepo;
    private final RuleLevelRepo ruleLevelRepo;
    private final RuleLevelMapper ruleLevelMapper;
    private final RuleMapper ruleMapper;

    @Override
    public MatchingRuleRes findMatchingRule(LimitType limitType, Long requestTotalValue) {
        //TODO with valid Date
        List<Rule> validRules = ruleRepo.findAllByLimitType(limitType,
                Sort.by(Sort.Direction.DESC, "effectiveDate"));
        if (CollectionUtils.isEmpty(validRules)) {
            return null;
        } else {
            Rule validRule = validRules.get(0);
            List<RuleLevel> allLevels = ruleLevelRepo.findAllByRuleId(validRule.getId(),
                    Sort.by(Sort.Direction.ASC, "level"));

            List<RuleLevelDto> approvalLevels = new ArrayList<>();
            for (RuleLevel ruleLevel : allLevels) {
                if (requestTotalValue.longValue() > ruleLevel.getLimitValue().longValue()) {
                    approvalLevels.add(ruleLevelMapper.toDto(ruleLevel));
                } else {
                    approvalLevels.add(ruleLevelMapper.toDto(ruleLevel));
                    break;
                }
            }
            return MatchingRuleRes.builder()
                    .id(validRule.getId())
                    .approveLevels(approvalLevels)
                    .expiredDate(validRule.getExpiredDate())
                    .effectiveDate(validRule.getEffectiveDate())
                    .name(validRule.getName())
                    .build();
        }
    }

    @Override
    public Page<RuleDto> search(String name, OffsetDateTime effectiveFrom, OffsetDateTime effectiveTo, int page, int size) {
        Specification<Rule> specification = buildConditionSearch(name, effectiveFrom, effectiveTo);
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "effectiveDate"));
        Page<Rule> pageResult = ruleRepo.findAll(specification, pageRequest);
        return pageResult.map(ruleMapper::toDto);
    }

    @Override
    public InputStream exportToExcel(String name, OffsetDateTime effectiveFrom, OffsetDateTime effectiveTo) {
        //TODO in version 2
        return null;
    }

    @Override
    public boolean deleteBatch(List<String> ruleIds) {
        //TODO in version 2
        return false;
    }

    private Specification<Rule> buildConditionSearch(String name, OffsetDateTime effectiveFrom, OffsetDateTime effectiveTo) {
        Specification<Rule> specification = Specification.where((root, query, criteriaBuilder) -> null);

        if (StringUtils.isNotBlank(name)) {
            String queryName = "%" + VNCharacterUtils.addEscape(VNCharacterUtils.removeAccent(name).toLowerCase()) + "%";
            specification = specification.and((root, query, criteriaBuilder) -> criteriaBuilder
                    .like(root.get("nameNoAccent"),
                            queryName, '\\'));
        }

        //TODO searh with date
        return specification;
    }
}
