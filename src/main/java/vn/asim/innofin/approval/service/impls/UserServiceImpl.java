package vn.asim.innofin.approval.service.impls;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import vn.asim.innofin.approval.config.annotation.NoLogging;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.AppConstants;
import vn.asim.innofin.approval.entity.User;
import vn.asim.innofin.approval.exception.model.NotAuthorizedException;
import vn.asim.innofin.approval.repo.UserRepo;
import vn.asim.innofin.approval.service.UserService;
import vn.asim.innofin.approval.utils.Utils;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    @Value("${approval.token.expiration:864_000_000}")
    long EXPIRATION;

    private final UserRepo userRepo;

    @Override
    @NoLogging
    public String login(String username, String password) {
        Optional<User> userOpt = userRepo.findByUsername(username);
        if (!userOpt.isPresent()) {
            log.warn("User not found: {}", username);
            throw new NotAuthorizedException();
        }

        User user = userOpt.get();

        if (!Utils.checkPassword(password, user.getPassword())) {
            log.warn("Password not match for user: {}", username);
            throw new NotAuthorizedException();
        }
        user.setLastLoginAt(new Date());
        userRepo.save(user);
        return getToken(user);
    }

    @Override
    public void logout(JwtInfo jwtInfo) {

    }


    private String getToken(User user) {
        return JWT.create()
                .withSubject(user.getUsername())
                .withClaim("userId", user.getId())
                .withClaim(JwtInfo.FULLNAME_FIELD, user.getFullname())
                .withClaim(JwtInfo.EMAIL_FIELD, user.getEmail())
                .withClaim(JwtInfo.USER_FIELD, user.getUsername())
                .withClaim(JwtInfo.ROLES_FIELD, Arrays.asList(user.getRoles().split(",")))
                .withIssuedAt(new Date())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION))
                .sign(Algorithm.HMAC512(AppConstants.JWT_SECRET_KEY));
    }
}
