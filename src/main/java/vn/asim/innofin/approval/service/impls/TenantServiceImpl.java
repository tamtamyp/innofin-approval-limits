package vn.asim.innofin.approval.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import vn.asim.innofin.approval.dto.response.TenantResDto;
import vn.asim.innofin.approval.entity.TenantProfile;
import vn.asim.innofin.approval.repo.TenantRepo;
import vn.asim.innofin.approval.service.TenantService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class TenantServiceImpl implements TenantService {
    private final TenantRepo tenantRepo;

    @Override
    public List<TenantResDto> getAllTenants() {
        List<TenantProfile> tenants = tenantRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
        return tenants.stream().map(tenantProfile -> TenantResDto.builder()
                .name(tenantProfile.getName())
                .code(tenantProfile.getCode())
                .build()).collect(Collectors.toList());
    }
}
