package vn.asim.innofin.approval.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.asim.innofin.approval.client.finccp.FinCCPClient;
import vn.asim.innofin.approval.client.finccp.dto.CashCollectionDetailv2;
import vn.asim.innofin.approval.client.finccp.dto.CollectorDetail;
import vn.asim.innofin.approval.client.finccp.dto.GroupDetail;
import vn.asim.innofin.approval.client.finccp.dto.ListData;
import vn.asim.innofin.approval.config.jwt.JwtInfo;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.constants.enums.RequestLimitStatus;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.Paginate;
import vn.asim.innofin.approval.dto.request.CreateRqApprovalLimitDto;
import vn.asim.innofin.approval.dto.request.RqApproveDto;
import vn.asim.innofin.approval.dto.request.UpdateRqApprovalLimitDto;
import vn.asim.innofin.approval.dto.request.UploadFileReqDto;
import vn.asim.innofin.approval.dto.response.*;
import vn.asim.innofin.approval.entity.*;
import vn.asim.innofin.approval.exception.model.*;
import vn.asim.innofin.approval.mappers.RequestLimitAcceptanceMapper;
import vn.asim.innofin.approval.mappers.RequestLimitMapper;
import vn.asim.innofin.approval.mappers.RequestLimitOrderMapper;
import vn.asim.innofin.approval.repo.*;
import vn.asim.innofin.approval.service.*;
import vn.asim.innofin.approval.utils.ExcelHelper;
import vn.asim.innofin.approval.utils.Utils;

import java.io.InputStream;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestLimitServiceImpl implements RequestLimitService {


    private final RuleRepo ruleRepo;
    private final RuleLevelRepo ruleLevelRepo;
    private final ApprovalRequestRepo requestLimitRepo;
    private final RequestLimitAcceptanceRepo requestLimitAcceptanceRepo;
    private final TenantRepo tenantRepo;
    private final RuleService ruleService;
    private final RequestLimitMapper requestLimitMapper;
    private final RequestLimitAcceptanceMapper requestLimitAcceptanceMapper;
    private final RequestLimitOrderRepo requestLimitOrderRepo;
    private final FileDataService fileDataService;
    private final FileDataRepo fileDataRepo;
    private final CommonService commonService;
    private final FinCCPClient finCCPClient;
    private final EmailService emailService;
    private final UserRepo userRepo;

    private final RequestLimitOrderMapper requestLimitOrderMapper;
    @Value("${approval.content.email.ref-link}")
    private String refLink;

    @Override
    public Page<RequestLimitRes> advancedSearch(List<String> requestSystem, LimitType limitType, List<String> collectorIds, List<RequestLimitStatus> statusList, Date createdDateFrom, Date createdDateTo, BigInteger limitFrom, BigInteger limitTo, int page, int size) {
        Specification<RequestLimit> searchCondition = buildConditionSearch(
                requestSystem, limitType, collectorIds, statusList,
                createdDateFrom, createdDateTo, limitFrom, limitTo);
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdDate"));
        Page<RequestLimit> pageResult = requestLimitRepo.findAll(searchCondition, pageRequest);
        Page<RequestLimitRes> result = pageResult.map(requestLimitMapper::toDto);
        attachLevelAcceptance(result.getContent());
        return result;
    }

    @Override
    public RequestLimitRes getDetails(Long id) {
        Optional<RequestLimit> requestLimitOpt = requestLimitRepo.findById(id);
        if (!requestLimitOpt.isPresent()) {
            log.warn("Not found Request Limit id {}", id);
            throw new NotExistException("Request Limit " + id + " is not exist");
        }
        RequestLimitRes result = requestLimitMapper.toDto(requestLimitOpt.get());
        attachLevelAcceptance(Collections.singletonList(result));
        // Attach order
        List<RequestLimitForOrder> orders = requestLimitOrderRepo.findByRequestLimitId(id);
        List<RequestLimitAcceptance> acceptances = requestLimitAcceptanceRepo.findAllByRequestLimitId(id, Sort.by(Sort.Direction.ASC,"level"));
        List<CashCollectionRes> orderRes =
                orders.stream().map(requestLimitOrderMapper::toDto).collect(Collectors.toList());
        List<RequestLimitLevelAcceptanceRes> acceptancesRes =
                acceptances.stream().map(requestLimitAcceptanceMapper::toDto).collect(Collectors.toList());
        result.setOrders(orderRes);
        result.setApprovals(acceptancesRes);


        List<FileData> files = fileDataRepo.findFileDataByRequestLimitId(id);
        List<FileResDto> fileRes = new ArrayList<>();
        files.forEach(d->{
            FileResDto array = new FileResDto();
            array.setFileName(d.getFileName());
            array.setFileDownloadCode(d.getFileCode());
            fileRes.add(array);
        });
        result.setFiles(fileRes);
        return result;
    }

    private Specification<RequestLimit>
    buildConditionSearch(List<String> systems,
                         LimitType limitType,
                         List<String> collectorId,
                         List<RequestLimitStatus> statusList,
                         Date createdDateFrom,
                         Date createdDateTo,
                         BigInteger limitFrom,
                         BigInteger limitTo) {
        Specification<RequestLimit> specification = Specification.where((root, query, criteriaBuilder) -> null);
        if (CollectionUtils.isNotEmpty(systems)) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    root.get("requestSystem").in(systems)
            );
        }

        if (limitType != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.equal(root.get("limitType"), limitType)
            );
        }

        if (CollectionUtils.isNotEmpty(collectorId)) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    root.get("collectorId").in(collectorId)
            );
        }

        if (CollectionUtils.isNotEmpty(statusList)) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    root.get("status").in(statusList)
            );
        }

        if (createdDateFrom != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.greaterThanOrEqualTo(root.get("createdDate"), createdDateFrom)
            );
        }

        if (createdDateTo != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.lessThanOrEqualTo(root.get("createdDate"), createdDateTo)
            );
        }

        if (limitFrom != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.greaterThanOrEqualTo(root.get("requestValue"), limitFrom)
            );
        }

        if (limitTo != null) {
            specification = specification.and((root, query, criteriaBuilder) ->
                    criteriaBuilder.lessThanOrEqualTo(root.get("requestValue"), limitTo)
            );
        }

        return specification;
    }

    private void attachLevelAcceptance(List<RequestLimitRes> requestLimitRes) {
        List<Long> idsRequestLimits = requestLimitRes.stream().map(rq -> rq.getId()).collect(Collectors.toList());
        List<RequestLimitAcceptance> allLevels = requestLimitAcceptanceRepo.findAllByRequestLimitIdIn(idsRequestLimits,
                Sort.by(Sort.Direction.ASC, "level"));
        Map<Long, RequestLimitRes> mapIdWithRq = requestLimitRes.stream()
                .collect(Collectors.toMap(RequestLimitRes::getId, Function.identity()));
        for (RequestLimitAcceptance acc : allLevels) {
            RequestLimitRes rqRes = mapIdWithRq.get(acc.getRequestLimitId());
            List<RequestLimitLevelAcceptanceRes> approvals = rqRes.getApprovals();
            if (approvals == null) {
                approvals = new ArrayList<>();
                rqRes.setApprovals(approvals);
            }
            approvals.add(requestLimitAcceptanceMapper.toDto(acc));
        }
    }

    private long getUnpaidAmount(Long requestLimitId) {
        Long sum = 0l;
        List<RequestLimitForOrder> allMountRecover = requestLimitOrderRepo.findByRequestLimitId(requestLimitId);
        for(RequestLimitForOrder x: allMountRecover) {
            if (x.getAmountRecover() != null) {
                sum += x.getAmountRecover();
            }
        }
        return sum;
    }

    @Override
    @Transactional
    public RequestLimitRes create(JwtInfo jwtInfo, CreateRqApprovalLimitDto requestApprovalLimits) {
        TenantProfile tenantProfile = commonService.verifyTenant(requestApprovalLimits.getRequestSystem());

        MatchingRuleRes matchingRule =
                ruleService.findMatchingRule(requestApprovalLimits.getLimitType(), requestApprovalLimits.getRequestTotalValue());
        if (!matchingRule.getId().equals(requestApprovalLimits.getMatchingRule())) {
            log.warn("MatchingRule is not valid");
            throw new BadParameterException("MatchingRule is not valid");
        }

        List<RequestLimit> listRequestLimitId = this.requestLimitRepo.findRequestLimitByCollectorId(requestApprovalLimits.getCollectorId());
        List<RequestLimit> listRequestLimitIdByStatus = listRequestLimitId.stream().filter(x->x.getStatus().equals(RequestLimitStatus.IN_WAITING)).collect(Collectors.toList());
        if (listRequestLimitIdByStatus.size() > 0) {
            throw new NotExistException("Can not create new request because has request status in waiting");
        }

        List<Long> listRequestLimitIds = listRequestLimitId.stream().filter(x->x.getStatus() == RequestLimitStatus.APPROVED)
                .map(o->o.getId()).distinct().collect(Collectors.toList());
        if (listRequestLimitIds.size() > 0) {
            List<RequestLimitForOrder> listRequestLimitForOrder = this.requestLimitOrderRepo.findAllByRequestLimitIdIn(listRequestLimitIds);
            if (listRequestLimitForOrder.size() > 0) {
                for (RequestLimitForOrder requestLimitForOrderItem: listRequestLimitForOrder) {
                    if (!requestLimitForOrderItem.isRegainStatus()) {
                        throw new NotExistException("Không thể tạo mới yêu cầu vì chưa thu hồi hết hạn mức cũ");
                    }
                }
            }
        }

        //Verify order
        List<CashCollectionRes> listCashCollectionExist = verifyOrders(tenantProfile,
                requestApprovalLimits.getCollectorId(), requestApprovalLimits.getLimitType(), requestApprovalLimits.getOrders());
        //Check order is exist in others request limit
        List<RequestLimitForOrder> duplicateOrder =  requestLimitOrderRepo
                .findAllByCodeIn(requestApprovalLimits.getOrders());
        if(duplicateOrder.size() != 0 ){
            List<Long> requestLimitIdDup = duplicateOrder.stream()
                    .map(o->o.getRequestLimitId()).distinct().collect(Collectors.toList());
            List<RequestLimit> dupRequestLimit = requestLimitRepo.findAllByIdIn(requestLimitIdDup);
            dupRequestLimit.forEach(requestLimit -> {
                if(!RequestLimitStatus.REJECTED.equals(requestLimit.getStatus())){
                    log.warn("order codes {} is exist in other request",duplicateOrder.toArray());
                    RequestLimitForOrder expError = duplicateOrder.get(0);
                    throw new BadParameterException("cash collections code " + expError.getCode()
                            +" is exist in request id "+ expError.getRequestLimitId());
                }
            });
        }

        boolean isAutoAccept = true;
        List<RequestLimitAcceptance> acceptances = new ArrayList<>();
        for (RuleLevelDto ruleLevelDto : matchingRule.getApproveLevels()) {
            acceptances.add(RequestLimitAcceptance.builder()
                    .level(ruleLevelDto.getLevel())
                    .approvedByRole(ruleLevelDto.getApprovedByRole())
                    .approvedTime(null)
                    .approvedBy(null)
                    .build());
            if (isAutoAccept && ruleLevelDto.getApprovedByRole() != null) {
                isAutoAccept = false;
            }
        }
        Object collector = this.verifyCollector(tenantProfile, requestApprovalLimits.getLimitType(), requestApprovalLimits.getCollectorId());


        Double originalCurrentLimit = 0d;
        Double originalRemainLimit = 0d;
        if(collector instanceof CollectorDetail){
            CollectorDetail collectorDetail = (CollectorDetail) collector;
            originalCurrentLimit = collectorDetail.getCollector().getLimitPoint() * 1000;
            originalRemainLimit = collectorDetail.getWallet().getPoint() * 1000;

        }else if(collector instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collector;
            originalCurrentLimit = group.getTotalLimit() * 1000d;
            originalRemainLimit = group.getRemainAmount() * 1000d;
        }

        String collectorName = getNameOfGroupOrCollector(collector);
        Date startWorkingDate = getStartWorkingDate(collector);
        this.verifyRequestAmount(collector,
                requestApprovalLimits.getRequestValue(),
                requestApprovalLimits.getRequestTotalValue());

        RequestLimit requestLimit = RequestLimit.builder()
                .status(isAutoAccept ? RequestLimitStatus.APPROVED : RequestLimitStatus.IN_WAITING)
                .note(requestApprovalLimits.getNote())
                .requestSystem(requestApprovalLimits.getRequestSystem())
                .createdBy(jwtInfo.getUser())
                .collectorName(collectorName)
                .matchingRuleId(requestApprovalLimits.getMatchingRule())
                .limitType(requestApprovalLimits.getLimitType())
                .collectorId(requestApprovalLimits.getCollectorId())
                .requestValue(requestApprovalLimits.getRequestValue())
                .requestTotalValue(requestApprovalLimits.getRequestTotalValue())
                .startWorkingDate(startWorkingDate)
                .createdDate(new Date())
                .originCurrentLimit(originalCurrentLimit)
                .originRemainLimit(originalRemainLimit)
                .build();

        RequestLimit newRequestLimit = requestLimitRepo.save(requestLimit);

        for (RequestLimitAcceptance acceptance : acceptances) {
            acceptance.setRequestLimitId(newRequestLimit.getId());
            if (acceptance.getApprovedByRole() == null) {
                acceptance.setApproved(true);
                acceptance.setApprovedTime(new Date());
                acceptance.setApprovedBy("AUTO");
            }
        }

        List<String> cashCollectionId = new ArrayList<>();
        requestLimitAcceptanceRepo.saveAll(acceptances);
        List<RequestLimitForOrder> requestLimitForOrders = listCashCollectionExist.stream().map(
                res->{
                    String historyId = res.getId();
                    UUID uuid = UUID.randomUUID();
                    res.setId(uuid.toString());
                    RequestLimitForOrder requestLimitForOrder = requestLimitOrderMapper.toEntity(res);
                    requestLimitForOrder.setHistoryId(historyId);
                    requestLimitForOrder.setRequestLimitId(requestLimit.getId());
                    cashCollectionId.add(historyId);
                    return requestLimitForOrder;
                }
        ).collect(Collectors.toList());

        Object collectors = this.verifyCollector(commonService.verifyTenant(requestLimit.getRequestSystem()), requestLimit.getLimitType(), requestLimit.getCollectorId());
        Long amountWallet = 0l;
        if(collectors instanceof CollectorDetail){
            CollectorDetail collectorDetail = (CollectorDetail) collectors;
            amountWallet = (long) (collectorDetail.getWallet().getPoint() * 1000);

        }else if(collectors instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collectors;
            amountWallet = group.getRemainAmount();
        }
        if (collectors != null) {
            for (RequestLimitForOrder item : requestLimitForOrders) {
                if (amountWallet > 0) {
                    if (item.getAmount() <= amountWallet) {
                        item.setAmountAllocate(0l);
                    } else {
                        item.setAmountAllocate(item.getAmount() - amountWallet);
                    }
                    amountWallet = amountWallet - item.getAmount();
                } else {
                    item.setAmountAllocate(item.getAmount());
                }
            }
        }
        requestLimitOrderRepo.saveAll(requestLimitForOrders);

        //Build Result
        RequestLimitRes requestLimitRes = requestLimitMapper.toDto(newRequestLimit);

        requestLimitRes.setApprovals(
                acceptances.stream().map(requestLimitAcceptanceMapper::toDto).collect(Collectors.toList())
        );

        if(requestApprovalLimits.getFiles() != null){
            List<FileResDto> fileRes = new ArrayList<>();
            for(UploadFileReqDto fileRequest: requestApprovalLimits.getFiles()){
                fileRes.add(FileResDto.builder()
                        .fileName(fileRequest.getFileName())
                        .fileDownloadCode(fileDataService.saveFile(requestLimitRes.getId(), fileRequest.getFileName(), fileRequest.getFileData()))
                        .build());
            }
            requestLimitRes.setFiles(fileRes);
        }
        if(isAutoAccept){
            finCCPClient.updateLimit(tenantProfile,
                    requestApprovalLimits.getCollectorId(),
                    requestApprovalLimits.getLimitType(), requestApprovalLimits.getRequestTotalValue());
            // send email  for the creater
            this.sendApprovedEmail(jwtInfo.getEmail(),
                    jwtInfo.getFullName(),
                    collectorName,
                    requestApprovalLimits.getRequestValue(),
                    requestApprovalLimits.getRequestTotalValue(),
                    requestLimit.getId());
        }else{
            // Send email for in-charge roles
            // Khi khoi tao request thi se gui email cho level 1
            List<RequestLimitAcceptance> filterRequestLimitAcceptances = acceptances.stream().filter(a->a.getApprovedByRole()!= null && a.getApprovedBy() == null && a.getLevel() == 1).collect(Collectors.toList());
            String roleCanApprove = filterRequestLimitAcceptances.get(0).getApprovedByRole();
            List<User> usersInCharge = userRepo.findAllByRoles(roleCanApprove);
            usersInCharge.forEach(user -> {
                this.sendRequest(user.getEmail(),
                        user.getFullname(),
                        collectorName,
                        requestApprovalLimits.getRequestValue(),
                        requestApprovalLimits.getRequestTotalValue(),
                        requestApprovalLimits.getNote(),
                        requestLimit.getId());
            });
//            List<String> rolesCanApprove = acceptances.stream()
//                            .filter(a->a.getApprovedByRole()!= null)
//                            .map(a->a.getApprovedByRole())
//                            .collect(Collectors.toList());
//            List<User> usersInCharge = userRepo.findAllByRolesIn(rolesCanApprove);
//            usersInCharge.forEach(user -> {
//                this.sendRequest(user.getEmail(),
//                        user.getFullname(),
//                        collectorName,
//                        requestApprovalLimits.getRequestValue(),
//                        requestApprovalLimits.getRequestTotalValue(),
//                        requestApprovalLimits.getNote());
//            });
        }
        if (requestLimit.getCollectorId() != null && cashCollectionId.size() > 0) {
            this.finCCPClient.checkHold(tenantProfile, requestApprovalLimits.getCollectorId(), cashCollectionId, true);
        }
        return requestLimitRes;
    }

    private Date getStartWorkingDate(Object collectorObj) {
        if(collectorObj instanceof CollectorDetail){
            CollectorDetail collector = (CollectorDetail) collectorObj;
            return collector.getCollector().getCreationTime();
        }else if(collectorObj instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collectorObj;
            return group.getCreationTime();
        } else {
            return null;
        }
    }

    private Date getCurrentTotalLimit(Object collectorObj) {
        if(collectorObj instanceof CollectorDetail){
            CollectorDetail collector = (CollectorDetail) collectorObj;
            return collector.getCollector().getCreationTime();
        }else if(collectorObj instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collectorObj;
            return group.getCreationTime();
        } else {
            return null;
        }
    }

    private Double getOriginalCurrentLimit(Object collectorObj) {
        if(collectorObj instanceof CollectorDetail){
            CollectorDetail collector = (CollectorDetail) collectorObj;
            return collector.getCollector().getLimitPoint() * 1000;
        }else if(collectorObj instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collectorObj;
            return group.getTotalLimit().doubleValue() * 1000;
        } else {
            return null;
        }
    }

    private Double getOriginalRemainLimit(Object collectorObj) {
        return null;
    }

    /**
     * Verify requestValue and requestTotalValue fit with orders
     * @param collector
     * @param requestValue
     * @param requestTotalValue
     */
    private void verifyRequestAmount(Object collector, Long requestValue, Long requestTotalValue) {
        //TODO: Check in auto withdraw limit
    }

    private List<CashCollectionRes> verifyOrders(TenantProfile tenantProfile, String collectorId, LimitType limitType, List<String> orderCodes){
        int currentCheckTime = 0;
        int maximumCheckTime = 3;
        int maximumInPage = 20;
        List<CashCollectionRes> finalResult = new ArrayList<>();
        Set<String> codeSet = orderCodes.stream().collect(Collectors.toSet());
        while(currentCheckTime < maximumCheckTime){
            ListData<CashCollectionDetailv2> result = finCCPClient.getCashCollectionv2(tenantProfile, collectorId, LimitType.GROUP.equals(limitType),null, null, currentCheckTime, maximumInPage);
            if(result == null){
                log.error("Error when call {} to get CashCollection to verify", tenantProfile.getName());
                throw new RemoteServerException("Không thể lấy dữ liệu từ "+tenantProfile.getName());
            }
            if(result.getItems().size() < maximumInPage ){
                currentCheckTime = maximumCheckTime;
            }else{
                currentCheckTime++;
            }
            result.getItems().forEach(cashCollectionDetailv2 -> {
                if(codeSet.contains(cashCollectionDetailv2.getCode())){
                    finalResult.add(requestLimitOrderMapper.fromFinCppDto(cashCollectionDetailv2));
                    codeSet.remove(cashCollectionDetailv2.getCode());
                }
            });
            if(finalResult.size() == orderCodes.size()){
                log.info("enough result");
                return finalResult;
            }
        }
        log.warn("Some orders is not used to request limit {}", codeSet);
        throw new BadParameterException("orders "+ codeSet + " is not used to request limit");

    }

    private String getNameOfGroupOrCollector(Object collectorObj){
        StringBuilder nameBuilder = new StringBuilder();
        if(collectorObj instanceof CollectorDetail){
            CollectorDetail collector = (CollectorDetail) collectorObj;
            if(collector.getAppUser() != null){
                if(collector.getAppUser().getSurname() != null){
                    nameBuilder.append(collector.getAppUser().getSurname().trim());
                }
                if(collector.getAppUser().getName() != null){
                    nameBuilder.append(" ").append(collector.getAppUser().getName().trim());
                }
            }
        }else if(collectorObj instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collectorObj;
            if(group.getGroupCollector() != null && group.getGroupCollector().getName() != null){
                nameBuilder.append(group.getGroupCollector().getName().trim());
            }
        }
        return nameBuilder.toString();
    }

    @Override
    @Transactional
    public RequestLimitRes update(JwtInfo jwtInfo, Long requestLimitId, UpdateRqApprovalLimitDto requestApprovalLimits) {
        Optional<RequestLimit> oldRequestLimitOpt = requestLimitRepo.findById(requestLimitId);
        RequestLimit oldRequestLimit = oldRequestLimitOpt.get();
        if(!RequestLimitStatus.IN_WAITING.equals(oldRequestLimit.getStatus())){
            log.warn("Can not update request limit {}. In state {}", requestLimitId, oldRequestLimit.getStatus());
            throw new BadParameterException("Can not update item "+ requestLimitId);
        }
        if(!oldRequestLimitOpt.isPresent()){
            log.warn("Request limit with id {} is not exist", requestLimitId);
            throw new NotExistException("Request limit is not exist");
        }

        TenantProfile tenantProfile = commonService.verifyTenant(requestApprovalLimits.getRequestSystem());
        //Verify order
        List<CashCollectionRes> listCashCollectionExist = verifyOrders(tenantProfile,
                requestApprovalLimits.getCollectorId(), requestApprovalLimits.getLimitType(), requestApprovalLimits.getOrders());
        //Check order is exist in others request limit
        List<RequestLimitForOrder> duplicateOrder =  requestLimitOrderRepo
                .findAllByCodeInAndRequestLimitIdIsNot(requestApprovalLimits.getOrders(), requestLimitId);
        if(duplicateOrder.size() != 0){
            List<Long> requestLimitIdDup = duplicateOrder.stream()
                    .map(o->o.getRequestLimitId()).distinct().collect(Collectors.toList());
            List<RequestLimit> dupRequestLimit = requestLimitRepo.findAllByIdIn(requestLimitIdDup);
            dupRequestLimit.forEach(requestLimit -> {
                if(!RequestLimitStatus.REJECTED.equals(requestLimit.getStatus())){
                    log.warn("order codes {} is exist in other request",duplicateOrder.toArray());
                    RequestLimitForOrder expError = duplicateOrder.get(0);
                    throw new BadParameterException("cash collections code " + expError.getCode()
                            +" is exist in request id "+ expError.getRequestLimitId());
                }
            });
        }
        List<RequestLimitForOrder> requestLimitForOrdersForUnHold = requestLimitOrderRepo.findByRequestLimitId(requestLimitId);
        //Delete old
        requestLimitAcceptanceRepo.deleteByRequestLimitId(requestLimitId);
        requestLimitOrderRepo.deleteByRequestLimitId(requestLimitId);
        if(requestApprovalLimits.getDeletedFiles() != null && requestApprovalLimits.getDeletedFiles().size() > 0){
            requestApprovalLimits.getDeletedFiles().forEach(d->{
                fileDataService.deleteFileOfRequestLimitAndFileCode(requestLimitId, d);
            });
        }


        MatchingRuleRes matchingRule =
                ruleService.findMatchingRule(requestApprovalLimits.getLimitType(), requestApprovalLimits.getRequestTotalValue());
        if (!matchingRule.getId().equals(requestApprovalLimits.getMatchingRule())) {
            log.warn("MatchingRule is not valid");
            throw new BadParameterException("MatchingRule is not valid");
        }

        boolean isAutoAccept = true;
        List<RequestLimitAcceptance> acceptances = new ArrayList<>();
        for (RuleLevelDto ruleLevelDto : matchingRule.getApproveLevels()) {
            RequestLimitAcceptance acceptance = RequestLimitAcceptance.builder()
                    .level(ruleLevelDto.getLevel())
                    .approvedByRole(ruleLevelDto.getApprovedByRole())
                    .approvedTime(null)
                    .approvedBy(null)
                    .requestLimitId(requestLimitId)
                    .build();
            acceptances.add(acceptance);
            if (acceptance.getApprovedByRole() == null) {
                acceptance.setApproved(true);
                acceptance.setApprovedTime(new Date());
                acceptance.setApprovedBy("AUTO");
            }
            if (isAutoAccept && ruleLevelDto.getApprovedByRole() != null) {
                isAutoAccept = false;
            }
        }
        requestLimitAcceptanceRepo.saveAll(acceptances);

        Object collector = this.verifyCollector(tenantProfile, requestApprovalLimits.getLimitType(), requestApprovalLimits.getCollectorId());
        String collectorName = getNameOfGroupOrCollector(collector);

        Double originalCurrentLimit = 0d;
        Double originalRemainLimit = 0d;
        if(collector instanceof CollectorDetail){
            CollectorDetail collectorDetail = (CollectorDetail) collector;
            originalCurrentLimit = collectorDetail.getCollector().getLimitPoint() * 1000;
            originalRemainLimit = collectorDetail.getWallet().getPoint() * 1000;

        }else if(collector instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collector;
            originalCurrentLimit = group.getTotalLimit() * 1000d;
            originalRemainLimit = group.getRemainAmount() * 1000d;
        }

        RequestLimit updatedRequestLimit = RequestLimit.builder()
                .id(requestLimitId)
                .status(isAutoAccept ? RequestLimitStatus.APPROVED : RequestLimitStatus.IN_WAITING)
                .note(requestApprovalLimits.getNote())
                .requestTotalValue(requestApprovalLimits.getRequestTotalValue())
                .requestSystem(requestApprovalLimits.getRequestSystem())
                .createdBy(oldRequestLimit.getCreatedBy())
                .updatedDate(new Date())
                .updatedBy(jwtInfo.getUser())
                .matchingRuleId(requestApprovalLimits.getMatchingRule())
                .limitType(requestApprovalLimits.getLimitType())
                .collectorId(requestApprovalLimits.getCollectorId())
                .requestValue(requestApprovalLimits.getRequestValue())
                .createdDate(oldRequestLimit.getCreatedDate())
                .collectorName(collectorName)
                .startWorkingDate(getStartWorkingDate(collector))
                .originCurrentLimit(originalCurrentLimit)
                .originRemainLimit(originalRemainLimit)
                .build();
        //update
        requestLimitRepo.save(updatedRequestLimit);
        List<String> cashCollectionId = new ArrayList<>();
        List<RequestLimitForOrder> requestLimitForOrders = listCashCollectionExist.stream().map(
                res->{
                    RequestLimitForOrder requestLimitForOrder = requestLimitOrderMapper.toEntity(res);
                    requestLimitForOrder.setRequestLimitId(updatedRequestLimit.getId());
                    requestLimitForOrder.setHistoryId(res.getId());
                    cashCollectionId.add(res.getId());
                    return requestLimitForOrder;
                }
        ).collect(Collectors.toList());

//        requestLimitOrderRepo.saveAll(
//                listCashCollectionExist.stream().map(cashCollection ->{
//                        RequestLimitForOrder requestLimitForOrder =
//                                requestLimitOrderMapper.toEntity(cashCollection);
//                        requestLimitForOrder.setRequestLimitId(updatedRequestLimit.getId());
//                        return requestLimitForOrder;
//                    }
//                ).collect(Collectors.toList()));

        Long amountWallet = 0l;
        if(collector instanceof CollectorDetail){
            CollectorDetail collectorDetail = (CollectorDetail) collector;
            amountWallet = (long) (collectorDetail.getWallet().getPoint() * 1000);

        }else if(collector instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collector;
            amountWallet = group.getRemainAmount();
        }

        if (collector != null) {
            for (RequestLimitForOrder item : requestLimitForOrders) {
                if (amountWallet > 0) {
                    if (item.getAmount() <= amountWallet) {
                        item.setAmountAllocate(0l);
                    } else {
                        item.setAmountAllocate(item.getAmount() - amountWallet);
                    }
                    amountWallet = amountWallet - item.getAmount();
                } else {
                    item.setAmountAllocate(item.getAmount());
                }
            }
        }
        requestLimitOrderRepo.saveAll(requestLimitForOrders);

        //Build Result
        RequestLimitRes requestLimitRes = requestLimitMapper.toDto(updatedRequestLimit);
//        Object collector = this.verifyCollector(tenantProfile, requestApprovalLimits.getLimitType(), requestApprovalLimits.getCollectorId());
//        String collectorName = getNameOfGroupOrCollector(collector);
        requestLimitRes.setCollectorName(collectorName);
        requestLimitRes.setApprovals(
                acceptances.stream().map(requestLimitAcceptanceMapper::toDto).collect(Collectors.toList())
        );
        List<FileResDto> fileRes = new ArrayList<>();
        if(requestApprovalLimits.getFiles() != null && requestApprovalLimits.getFiles().size() > 0){
            for(UploadFileReqDto fileRequest: requestApprovalLimits.getFiles()){
                fileRes.add(FileResDto.builder()
                        .fileName(fileRequest.getFileName())
                        .fileDownloadCode(fileDataService.saveFile(requestLimitRes.getId(), fileRequest.getFileName(), fileRequest.getFileData()))
                        .build());
            }
        }
        List<FileData> files = fileDataRepo.findFileDataByRequestLimitId(requestLimitId);
        if(files.size()>0){
            List<FileResDto> fileDataRes = new ArrayList<>();
            files.forEach(d->{
                FileResDto array = new FileResDto();
                array.setFileName(d.getFileName());
                array.setFileDownloadCode(d.getFileCode());
                fileDataRes.add(array);
            });
            requestLimitRes.setFiles(fileDataRes);
        }
        if(isAutoAccept){
            //TODO send approved email for creater of limit request. @luongnguyen2
        }

        List<String>unHoldId = new ArrayList<>();
        if (requestLimitForOrdersForUnHold.size() > 0) {
            for (RequestLimitForOrder unHoldItem: requestLimitForOrdersForUnHold) {
                unHoldId.add(unHoldItem.getHistoryId());
            }
        }
        if (unHoldId.size() > 0) {
            boolean checkHold = this.finCCPClient.checkHold(tenantProfile, requestApprovalLimits.getCollectorId(), unHoldId, false);
            if (cashCollectionId.size() > 0) {
                if (checkHold == true) {
                    this.finCCPClient.checkHold(tenantProfile, requestApprovalLimits.getCollectorId(), cashCollectionId, true);
                }
            }
        }

        return requestLimitRes;
    }

    @Override
    @Transactional
    public void deleteBatch(List<Long> ids) {
        List<RequestLimit> requests = requestLimitRepo.findAllByIdIn(ids);
        if (requests.size() != ids.size()) {
            log.warn("Some id which request to delete is not exists. {}", ids);
            throw new BadParameterException("Some id which request to delete is not exists");
        }

        for(RequestLimit deleteRequest: requests){
            if(!RequestLimitStatus.IN_WAITING.equals(deleteRequest.getStatus())){
                log.warn("Can not delete request {} in status {}",deleteRequest.getId(), deleteRequest.getStatus());
                throw new BadParameterException("Can not delete request not in status "+ RequestLimitStatus.IN_WAITING.name());
            }
        }

        requestLimitRepo.deleteAllById(ids);
        requestLimitAcceptanceRepo.deleteAllByRequestLimitIdIn(ids);
    }

    @Override
    public InputStream exportToExcel(List<String> requestSystems,
                                     LimitType limitType,
                                     List<String> collectorIds,
                                     List<RequestLimitStatus> statusList,
                                     Date createdDateFrom,
                                     Date createdDateTo,
                                     BigInteger limitFrom,
                                     BigInteger limitTo) {
        Specification<RequestLimit> searchCondition = buildConditionSearch(
                requestSystems , limitType, collectorIds, statusList,
                createdDateFrom, createdDateTo, limitFrom, limitTo);
        List<RequestLimit> requestLimits = requestLimitRepo.findAll(searchCondition, Sort.by(Sort.Direction.DESC, "createdDate"));
        List<RequestLimitRes> results = requestLimits.stream().map(requestLimitMapper::toDto).collect(Collectors.toList());
        attachLevelAcceptance(results);

        String[] headers = new String[]{
                "Loại hạn mức",
                "Người yêu cầu",
                "Số tiền",
                "Trạng thái",
                "Người duyệt cấp 0",
                "Người duyệt cấp 1",
                "Người duyệt cấp 2",
                "Người duyệt cấp 3"
        };
        String[][] dataList = new String[results.size()][8];
        for(int line=0; line < results.size(); line++){
            RequestLimitRes dataObj = results.get(line);
            List<RequestLimitLevelAcceptanceRes> approvalDataLst = results.get(line).getApprovals();
            String aprLevel0 = "Tự động";
            String aprLevel1 = approvalDataLst.size()>1?
                    commonService.getRoleNameByCode(approvalDataLst.get(1).getApprovedByRole()):
                    "";
            String aprLevel2 = approvalDataLst.size()>2?
                    commonService.getRoleNameByCode(approvalDataLst.get(2).getApprovedByRole()):
                    "";
            String aprLevel3 = approvalDataLst.size()>3?
                    commonService.getRoleNameByCode(approvalDataLst.get(3).getApprovedByRole()):
                    "";

            dataList[line][0] = commonService.getLimitTypeNameByCode(dataObj.getLimitType().getCode());
            dataList[line][1] = dataObj.getCollectorName();
            dataList[line][2] = Utils.formatCurrency(dataObj.getRequestValue()+"");
            dataList[line][3] = commonService.getRequestStatusNameByCode(dataObj.getStatus().getCode());
            dataList[line][4] = aprLevel0;
            dataList[line][5] = aprLevel1;
            dataList[line][6] = aprLevel2;
            dataList[line][7] = aprLevel3;
        }
        return ExcelHelper.exportToExcel(headers, dataList);
    }

    public RequestLimitAcceptance getLevelNeedApprove(List<RequestLimitAcceptance> allLevels){
        for(int i =0 ; i < allLevels.size(); i++){
            RequestLimitAcceptance thisLevel = allLevels.get(i);
            if(thisLevel.getApprovedBy() == null){
                return thisLevel;
            }
        }
        return null;
    }

    @Override
    @Transactional
    public void approve(JwtInfo jwtInfo, RqApproveDto requestApprovalLimits) {
        Optional<RequestLimit> requestLimitOpt =
                requestLimitRepo.findById(requestApprovalLimits.getRequestId());
        if (!requestLimitOpt.isPresent()) {
            log.warn("Request limit id {} is not exist", requestApprovalLimits.getRequestId());
            throw new NotExistException("Request limit is not exist");
        }

        RequestLimit requestLimit = requestLimitOpt.get();
        if(RequestLimitStatus.APPROVED.equals(requestLimit.getStatus()) ||
                RequestLimitStatus.REJECTED.equals(requestLimit.getStatus())){
            log.warn("Can not approve Request limit in status: {}", requestLimit.getStatus());
            throw new BadParameterException("Request limit in status "+ requestLimit.getStatus()+" is not valid");
        }

        List<RequestLimitAcceptance> requestLimitAcceptances =
                requestLimitAcceptanceRepo.findAllByRequestLimitId(requestLimit.getId(), Sort.by(Sort.Direction.ASC,"level"));
        RequestLimitAcceptance currentLevelNeedApprove = this.getLevelNeedApprove(requestLimitAcceptances);
        if(currentLevelNeedApprove == null){
            log.error("All level is approved");
            throw new ServiceException("Data is not valid");
        }
        if(!jwtInfo.getRoles().contains(currentLevelNeedApprove.getApprovedByRole())){
            log.warn("Required role {}", currentLevelNeedApprove.getApprovedByRole());
            throw new BadParameterException("Required role "+ currentLevelNeedApprove.getApprovedByRole() +" at level "+currentLevelNeedApprove.getLevel());
        }
        currentLevelNeedApprove.setApprovedTime(new Date());
        currentLevelNeedApprove.setApproved(requestApprovalLimits.getIsApprove());
        currentLevelNeedApprove.setUpdatedDate(new Date());
        currentLevelNeedApprove.setApprovedBy(jwtInfo.getUser());
        currentLevelNeedApprove.setNote(requestApprovalLimits.getNote());


        boolean isLatestLevel = true;
        for(int i = requestLimitAcceptances.size() -1 ; i >=0; i--){
            RequestLimitAcceptance thisLevel = requestLimitAcceptances.get(i);
            if(thisLevel.getApprovedBy() == null){
                isLatestLevel = false;
                break;
            }
        }
        changeStateOfRequestLimit(jwtInfo.getUser(), requestLimit, currentLevelNeedApprove, isLatestLevel);
        requestLimitRepo.save(requestLimit);
        requestLimitAcceptanceRepo.save(currentLevelNeedApprove);
        if(RequestLimitStatus.REJECTED.equals(requestLimit.getStatus())){
            // Rejected
            log.info("User {} rejected request {}", jwtInfo.getUser(), requestLimit.getId());
            String creater = requestLimit.getCreatedBy();
            Optional<User> userOpt = userRepo.findByUsername(creater);
            if (userOpt.isPresent()) {
                this.sendRejectedEmail(userOpt.get().getEmail(),
                        userOpt.get().getFullname(),
                        requestLimit.getCollectorName(),
                        requestLimit.getRequestValue(),
                        requestLimit.getRequestTotalValue(),
                        requestApprovalLimits.getNote(),
                        requestLimit.getId());
            }else{
                log.error("Can not find user with username: {}", creater);
                throw new NotExistException("User not exist: "+ creater);
            }
            List<String> cashCollectionId = new ArrayList<>();
            List<RequestLimitForOrder> requestLimitForOrders = this.requestLimitOrderRepo.findByRequestLimitId(requestLimit.getId());
            if (requestLimitForOrders.size() > 0) {
                for (RequestLimitForOrder ids : requestLimitForOrders) {
                    cashCollectionId.add(ids.getHistoryId());
                }
            }
            TenantProfile tenantProfile = commonService.verifyTenant(requestLimit.getRequestSystem());
            if (requestLimit.getCollectorId() != null && cashCollectionId.size() > 0) {
                this.finCCPClient.checkHold(tenantProfile, requestLimit.getCollectorId(), cashCollectionId, false);
            }
        } else {
            if(isLatestLevel){
                finCCPClient.updateLimit(commonService.verifyTenant(requestLimit.getRequestSystem()),
                        requestLimit.getCollectorId(), requestLimit.getLimitType(), requestLimit.getRequestTotalValue());

                // Send email for creater
                String creater = requestLimit.getCreatedBy();
                Optional<User> userOpt = userRepo.findByUsername(creater);
                if (userOpt.isPresent()) {
                    this.sendApprovedEmail(userOpt.get().getEmail(),
                            userOpt.get().getFullname(),
                            requestLimit.getCollectorName(),
                            requestLimit.getRequestValue(),
                            requestLimit.getRequestTotalValue(),
                            requestLimit.getId()
                    );
                }else{
                    log.error("Can not find user with username: {}", creater);
                    throw new NotExistException("User not exist: "+ creater);
                }
            } else {
                List<RequestLimitAcceptance> filterRequestLimitAcceptances = requestLimitAcceptances.stream().filter(a->a.getApprovedByRole()!= null && a.getApprovedBy() == null).collect(Collectors.toList());
                Optional<RequestLimitAcceptance> requestLimitAcceptanceMinLevel = filterRequestLimitAcceptances.stream().min(Comparator.comparing(RequestLimitAcceptance::getLevel));
                String roleCanApprove = requestLimitAcceptanceMinLevel.get().getApprovedByRole();
                List<User> usersInCharge = userRepo.findAllByRoles(roleCanApprove);
                usersInCharge.forEach(user -> {
                    this.sendRequest(user.getEmail(),
                            user.getFullname(),
                            requestLimit.getCollectorName(),
                            requestLimit.getRequestValue(),
                            requestLimit.getRequestTotalValue(),
                            requestApprovalLimits.getNote(),
                            requestLimit.getId());
                    });
            }
        }
    }

    private void changeStateOfRequestLimit(String user, RequestLimit requestLimit, RequestLimitAcceptance currentLevelNeedApprove, boolean isLatestLevel) {
        // If canceled
        if (!currentLevelNeedApprove.isApproved()) {
            requestLimit.setStatus(RequestLimitStatus.REJECTED);
        } else if(isLatestLevel){
            requestLimit.setStatus(RequestLimitStatus.APPROVED);
        } else{
            switch ((int) currentLevelNeedApprove.getLevel()){
                case 1 :
                    requestLimit.setStatus(RequestLimitStatus.APPROVED_LV1);
                    break;
                case 2 :
                    requestLimit.setStatus(RequestLimitStatus.APPROVED_LV2);
                    break;
                case 3 :
                    requestLimit.setStatus(RequestLimitStatus.APPROVED_LV3);
                    break;
                case 4 :
                    requestLimit.setStatus(RequestLimitStatus.APPROVED_LV4);
                    break;
                default:
                    log.error("Unknown level {}",currentLevelNeedApprove.getLevel());
                    throw new ServiceException("Level is not implements in this service:"+ currentLevelNeedApprove.getLevel());
            }
        }
        requestLimit.setUpdatedBy(user);
        requestLimit.setUpdatedDate(new Date());
    }


    /**
     * @param limitType
     * @param collectorId
     * @return Collector or CollectorGroup. Depend on @param limitType
     */
    private Object verifyCollector(TenantProfile tenantProfile, LimitType limitType, String collectorId) {
        if(LimitType.INDIVIDUAL.equals(limitType)){
            //Verify Collector
            CollectorDetail collectorDetail = finCCPClient.getCollectorById(tenantProfile, collectorId);
            if(collectorDetail == null){
                log.warn("Request limit for collector {} is not exist", collectorId);
                throw new NotExistException("Collector is not exist");
            }else{
                return collectorDetail;
            }
        }else{
            //Verify Group
            GroupDetail groupDetail = finCCPClient.getGroupById(tenantProfile, collectorId);
            if(groupDetail == null){
                log.warn("Request limit for group {} is not exist", collectorId);
                throw new NotExistException("Group is not exist");
            }else{
                return groupDetail;
            }
        }
    }

    @Override
    public Page<ApprovalHistoryResDto> getApproveHistory(LimitType limitType, String collectorId, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "createdDate"));
        Specification<RequestLimit> searchCondition = buildConditionSearch(
                null, limitType, Collections.singletonList(collectorId), null,
                null, null, null, null);
        Page<RequestLimit> pageResult = requestLimitRepo.findAll(searchCondition, pageRequest);
        Page<RequestLimitRes> result = pageResult.map(requestLimitMapper::toDto);
        attachLevelAcceptance(result.getContent());
        return result.map(rq -> ApprovalHistoryResDto.builder()
                .id(rq.getId())
                .requestDate(rq.getCreatedDate())
                .amount(rq.getRequestValue())
                .totalAmount(rq.getRequestTotalValue())
                .status(rq.getStatus())
                .note(rq.getNote())
                .unpaidMoney((rq.getRequestTotalValue() > getUnpaidAmount(rq.getId())) ? (rq.getRequestTotalValue() - getUnpaidAmount(rq.getId())) : 0)
                .approvalLevels(
                        rq.getApprovals().stream()
                                .map(al -> RequestLimitApprovalHis.builder()
                                        .approvedBy(al.getApprovedBy())
                                        .level(al.getLevel())
                                        .isApproved(al.isApproved())
                                        .approvedTime(al.getApprovedTime())
                                        .approvedByRole(al.getApprovedByRole())
                                        .build()).collect(Collectors.toList()))
                .build());
    }

    public BaseListResponse<DepositHistoryDto> getPaymentHistory(String requestSystem, String collectorId, LimitType limitType, Integer page, Integer size) {
        TenantProfile tenantProfile = commonService.verifyTenant(requestSystem);
        if (StringUtils.isEmpty(collectorId)) {
            throw new BadParameterException("CollectorId is not exist");
        }
        ListData<DepositHistoryFinCPPDto> result = finCCPClient.getPaymentHistory(tenantProfile, collectorId,LimitType.GROUP.equals(limitType), page, size, "creationTime");
        List<DepositHistoryDto> resultContent = result.getItems().stream().map(c -> {
                    return DepositHistoryDto.builder()
                            .id(c.getId())
                            .totalDeposit(c.getTotalCashCollection())
                            .bankName(c.getBankName())
                            .merchantName(c.getMerchantName())
                            .storeName(c.getStoreName())
                            .collectionCode(c.getCashCollectionCode())
                            .processTime(c.getProcessTime())
                            .creditTime(c.getCreditTime())
                            .build();
                }
        ).collect(Collectors.toList());
        BaseListResponse<DepositHistoryDto> baseListResponse = new BaseListResponse<>();
        baseListResponse.setData(resultContent);
        baseListResponse.setPaginate(Paginate.builder()
                .totalElement(result.getTotalCount())
                .totalPage((long) Math.ceil(result.getTotalCount()/(double)size))
                .size(size)
                .page(page)
                .build());
        return baseListResponse;
    }

    public List<Long> getAllocateAmount(String requestSystem, LimitType limitType, String collectorId, List<Double> orders){
        Object collectors = this.verifyCollector(commonService.verifyTenant(requestSystem), limitType, collectorId);
        Long amountWallet = 0l;
        if(collectors instanceof CollectorDetail){
            CollectorDetail collectorDetail = (CollectorDetail) collectors;
            amountWallet = (long) (collectorDetail.getWallet().getPoint() * 1000);

        }else if(collectors instanceof GroupDetail){
            GroupDetail group = (GroupDetail) collectors;
            amountWallet = group.getRemainAmount();
        }
        List <Long> amount = new ArrayList<>();
        if (orders != null) {
            for (int i=0; i < orders.size(); i++) {
                if (amountWallet > 0) {
                    if (orders.get(i).longValue() <= amountWallet) {
                        amount.add(0l);
                    } else {
                        amount.add(orders.get(i).longValue() - amountWallet);
                    }
                    amountWallet = amountWallet - orders.get(i).longValue();
                } else {
                    amount.add(orders.get(i).longValue());
                }
            }
        }
        return amount;
    }

    private void sendApprovedEmail(String to, String opsCreaterName, String collectorName, Long amount, Long total, Long requestLimitId){
        Map<String, Object> variables = new HashMap<>();
        variables.put("approvalName", opsCreaterName);
        variables.put("collectorName", collectorName);
        variables.put("amount", Utils.formatCurrency(amount+""));
        variables.put("total", Utils.formatCurrency(total+""));
        variables.put("refLink", refLink);
        variables.put("requestLimitId", requestLimitId);
        emailService.sendApprovedEmail(new String[]{to}, null, null, variables);
        log.info("Sent approved email to {}", to);
    }

    private void sendRejectedEmail(String to, String opsCreaterName, String collectorName, Long amount, Long total, String note, Long requestLimitId){
        Map<String, Object> variables = new HashMap<>();
        variables.put("approvalName", opsCreaterName);
        variables.put("collectorName", collectorName);
        variables.put("amount", Utils.formatCurrency(amount+""));
        variables.put("total", Utils.formatCurrency(total+""));
        variables.put("note", note);
        variables.put("refLink", refLink);
        variables.put("requestLimitId", requestLimitId);
        emailService.sendDeclinedEmail(new String[]{to}, null, null, variables);
        log.info("Sent Rejected email to {}", to);
    }

    private void sendRequest(String to, String opsCreaterName, String collectorName, Long amount, Long total, String note, Long requestLimitId){
        Map<String, Object> variables = new HashMap<>();
        variables.put("approvalName", opsCreaterName);
        variables.put("collectorName", collectorName);
        variables.put("amount", Utils.formatCurrency(amount+""));
        variables.put("total", Utils.formatCurrency(total+""));
        variables.put("refLink", refLink);
        variables.put("requestLimitId", requestLimitId);
        emailService.sendRequestApprovalEmail(new String[]{to}, null, null, variables);
        log.info("Sent Request Approval email to {}", to);
    }

}