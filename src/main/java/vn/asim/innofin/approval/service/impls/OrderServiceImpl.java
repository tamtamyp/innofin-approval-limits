package vn.asim.innofin.approval.service.impls;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import vn.asim.innofin.approval.client.finccp.FinCCPClient;
import vn.asim.innofin.approval.client.finccp.dto.CashCollectionDetailv2;
import vn.asim.innofin.approval.client.finccp.dto.ListData;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.constants.enums.RequestLimitStatus;
import vn.asim.innofin.approval.dto.BaseListResponse;
import vn.asim.innofin.approval.dto.Paginate;
import vn.asim.innofin.approval.dto.response.CashCollectionRes;
import vn.asim.innofin.approval.dto.response.MerchantDto;
import vn.asim.innofin.approval.entity.Merchant;
import vn.asim.innofin.approval.entity.RequestLimit;
import vn.asim.innofin.approval.entity.RequestLimitForOrder;
import vn.asim.innofin.approval.entity.TenantProfile;
import vn.asim.innofin.approval.repo.ApprovalRequestRepo;
import vn.asim.innofin.approval.repo.MerchantRepo;
import vn.asim.innofin.approval.repo.RequestLimitOrderRepo;
import vn.asim.innofin.approval.service.CommonService;
import vn.asim.innofin.approval.service.OrderService;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final CommonService commonService;

    private final MerchantRepo merchantRepo;

    private final FinCCPClient finCCPClient;

    private final RequestLimitOrderRepo requestLimitOrderRepo;

    private final ApprovalRequestRepo requestLimitRepo;
    @Override
    public BaseListResponse<CashCollectionRes> getOrders(String requestSystem, String collectorId, LimitType limitType, String code, String merchantId, int page, int size) {
        TenantProfile tenantProfile = commonService.verifyTenant(requestSystem);
        //Get Info from tenant
        ListData<CashCollectionDetailv2> rawCashCollection = finCCPClient.getCashCollectionv2(tenantProfile,
                collectorId,
                LimitType.GROUP.equals(limitType),
                code, merchantId, page, size);

        List<CashCollectionRes> resultContent = rawCashCollection.getItems().stream().map(c->
                CashCollectionRes.builder()
                        .id(c.getId())
                        .amount(c.getTotal())
                        .code(c.getCode())
                        .paymentTime(null)
                        .storeName(c.getStoreName())
                        .requestTime(c.getRequestDateTime())
                        .requestEndTime(c.getRequestEndTime())
                        .status(commonService.getCashCollectionStatusFromStatusId(c.getProcessStatus()))
                        .bankName(c.getBank()== null? null: c.getBank().getCode())
                        .merchantName(c.getMerchantName())
                        .isBlock(checkBlock(c.getCode()))
                        .build()

        ).collect(Collectors.toList());
        BaseListResponse<CashCollectionRes> baseListResponse = new BaseListResponse<>();
        baseListResponse.setData(resultContent);
        baseListResponse.setPaginate(Paginate.builder()
                .totalElement(rawCashCollection.getTotalCount())
                .totalPage((long) Math.ceil(rawCashCollection.getTotalCount()/(double)size))
                .size(size)
                .page(page)
                .build());
        return baseListResponse;
    }

    public boolean checkBlock(String code) {
        boolean isBlock = false;
        List<RequestLimitForOrder> duplicateOrder =  requestLimitOrderRepo
                .findAllByCode(code);
        if(duplicateOrder.size() != 0 ){
            List<Long> requestLimitIdDup = duplicateOrder.stream()
                    .map(o->o.getRequestLimitId()).distinct().collect(Collectors.toList());
            List<RequestLimit> dupRequestLimit = requestLimitRepo.findAllByIdIn(requestLimitIdDup);
            if (dupRequestLimit.size() > 0) {
                for (RequestLimit item: dupRequestLimit) {
                    if(!RequestLimitStatus.REJECTED.equals(item.getStatus())) {
                        isBlock = true;
                        break;
                    }
                }
            } else {
                isBlock = false;
            }
        }
        return isBlock;
    }

    public List<MerchantDto> getAllMerchants(String requestSystem){
        TenantProfile profile = commonService.verifyTenant(requestSystem);
        List<Merchant> alls = merchantRepo.findAllByTenant(profile.getCode(), Sort.by(Sort.Direction.ASC, "name"));
        return alls.stream().map(m->MerchantDto.builder()
                .code(m.getCode())
                .name(m.getName())
                .logo(m.getLogo())
                .id(m.getId())
                .build()).collect(Collectors.toList());
    }
}
