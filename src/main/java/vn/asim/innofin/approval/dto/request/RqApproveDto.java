package vn.asim.innofin.approval.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class RqApproveDto {
    @NotNull
    Long requestId;
    @NotNull
    private Boolean isApprove;

    @Size(max = 256)
    String note;

}
