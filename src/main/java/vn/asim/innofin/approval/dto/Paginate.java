package vn.asim.innofin.approval.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Paginate {

    long totalPage;
    long totalElement;

    int page;

    int size;
}
