package vn.asim.innofin.approval.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.*;

import java.math.BigInteger;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CollectorResDto {
    private String id;
    //    private String code;
    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    private Date startWorkingDate;
    private long warningCount;
    private Long currentLimit;

    private Long remainAmount;
}
