package vn.asim.innofin.approval.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.asim.innofin.approval.constants.enums.LimitType;
import vn.asim.innofin.approval.constants.enums.RequestLimitStatus;
import vn.asim.innofin.approval.entity.RequestLimitForOrder;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestLimitRes {

    @Schema(description = "Id of collector or group")
    Long id;
    String requestSystem;
    LimitType limitType;
    String collectorName;
    String collectorId;
    Long requestValue;
    Long requestTotalValue;
    RequestLimitStatus status;
    String note;
    List<RequestLimitLevelAcceptanceRes> approvals;
    List<CashCollectionRes> orders;

    List<FileResDto> files;

    long collectorWarningCount = 0;
    Double originCurrentLimit;
    Double originRemainLimit;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date createdDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date startWorkingDate;
}
