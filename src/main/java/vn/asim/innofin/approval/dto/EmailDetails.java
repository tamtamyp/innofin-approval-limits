package vn.asim.innofin.approval.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
@Builder
public class EmailDetails {
    private List<String> to;
    private List<String> bcc;
    private List<String> cc;
    private String title;
    private String content;


}
