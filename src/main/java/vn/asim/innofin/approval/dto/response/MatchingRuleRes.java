package vn.asim.innofin.approval.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import vn.asim.innofin.approval.dto.RuleDto;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class MatchingRuleRes extends RuleDto {
    List<RuleLevelDto> approveLevels;
}
