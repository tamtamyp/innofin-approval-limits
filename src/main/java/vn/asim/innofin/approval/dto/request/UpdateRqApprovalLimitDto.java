package vn.asim.innofin.approval.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import vn.asim.innofin.approval.constants.enums.LimitType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.util.List;


@Data
public class UpdateRqApprovalLimitDto {
    @Schema(description = "System send the request")
    @NotBlank
    String requestSystem;

    @Schema(description = "Limit type", implementation = LimitType.class)
    @NotNull
    LimitType limitType;

    @NotBlank
    String collectorId;

    @Schema(description = "Amount requested increase")
    @NotNull
    Long requestValue;

    @Schema(description = "Amount after requested increase")
    @NotNull
    Long requestTotalValue;

    @Schema(description = "Note")
    @Size(max = 500)
    String note;

    @Schema(description = "Approval Rule match ")
    @NotNull
    Long matchingRule;

    @Schema(description = "File info")
    private List<UploadFileReqDto> files;

    @Schema(description = "File delete info")
    private List<@NotBlank String> deletedFiles;

    @Schema(description = "Request for orderCode")
    @NotEmpty List<@NotBlank String> orders;
}
