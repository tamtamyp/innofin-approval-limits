package vn.asim.innofin.approval.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.asim.innofin.approval.constants.enums.CashCollectionProcessStatus;

import javax.persistence.Id;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CashCollectionRes {
    String id;
    String code;
    String merchantName;
    String storeName;
    String bankName;
    Long amount;
    CashCollectionProcessStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date requestTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date requestEndTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date paymentTime;
    boolean isBlock;
    Long amountRecover; // số tiền thu hồi
    Long amountAllocate; // số tiền cấp phát
}
