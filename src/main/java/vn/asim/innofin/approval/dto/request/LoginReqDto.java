package vn.asim.innofin.approval.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.asim.innofin.approval.config.annotation.Exclude;
import vn.asim.innofin.approval.config.annotation.NoLogging;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginReqDto {
    @NotBlank String username;

    @Exclude
    @NotBlank String password;
}
