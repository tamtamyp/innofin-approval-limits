package vn.asim.innofin.approval.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MerchantDto {
    public String id;
    public String code;
    public String name;
    public String logo;
}
