package vn.asim.innofin.approval.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BaseListResponse<T> {

    public Paginate paginate;
    public List<T> data = new ArrayList<>();

    public static BaseListResponse ofOriginalPage(Page oPage) {
        if (oPage == null) {
            return null;
        }
        return BaseListResponse.builder()
                .paginate(Paginate.builder()
                        .page(oPage.getNumber())
                        .totalPage(oPage.getTotalPages())
                        .size(oPage.getSize())
                        .totalElement(oPage.getTotalElements())
                        .build())
                .data(oPage.getContent())
                .build();
    }

}
