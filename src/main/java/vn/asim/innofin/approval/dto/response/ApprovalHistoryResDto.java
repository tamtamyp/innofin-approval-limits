package vn.asim.innofin.approval.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.asim.innofin.approval.constants.enums.RequestLimitStatus;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApprovalHistoryResDto {
    Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    Date requestDate;

    Long amount;

    Long totalAmount;

    RequestLimitStatus status;

    List<RequestLimitApprovalHis> approvalLevels;
    String note;

    Long unpaidMoney;
}
