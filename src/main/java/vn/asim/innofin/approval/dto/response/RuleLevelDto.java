package vn.asim.innofin.approval.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RuleLevelDto {
    String id;
    long level;
    BigInteger limitValue;
    String approvedByRole;

}
