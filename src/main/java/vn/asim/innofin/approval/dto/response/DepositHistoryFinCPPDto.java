package vn.asim.innofin.approval.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.*;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DepositHistoryFinCPPDto {
    private String id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601)
    private Date creationTime;
    private String cashCollectionId;
    private String collectorId;
    private Double totalDeposit;
    private Double totalCashCollection;
    private Date creditTime;
    private String merchantName;
    private String storeName;
    private String bankName;
    private Date processTime;
    private String cashCollectionCode;
}
