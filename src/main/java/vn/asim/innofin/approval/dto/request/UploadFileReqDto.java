package vn.asim.innofin.approval.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.asim.innofin.approval.config.annotation.Exclude;
import vn.asim.innofin.approval.constants.AppConstants;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileReqDto {

    @NotBlank
    private String fileName;

    @Size(min = 1, max = AppConstants.MAX_UPLOAD_SIZE)
    @NotNull
    @Exclude
    private byte[] fileData;

}
